package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;

import javazoom.jlgui.basicplayer.BasicPlayer;
import javazoom.jlgui.basicplayer.BasicPlayerException;

import modelo.Reproductor;
import vista.VistaAcertijo1;

public class ControladorAcertijo implements ActionListener {
VistaAcertijo1 vista=new VistaAcertijo1(this);
Reproductor re;
ArrayList<Integer> array = new ArrayList<Integer>();
String verificador="";
int cont=0;
Boolean rta=false;
public ControladorAcertijo() {
	this.vista = new VistaAcertijo1(this);
	this.vista.setVisible(true);
	
	
	
	
	
	
}


public VistaAcertijo1 getVista() {
	return vista;
}

public void setVista(VistaAcertijo1 vista) {
	this.vista = vista;
}

@Override
public void actionPerformed(ActionEvent e) {
   re=new Reproductor();
	if(e.getSource().equals(getVista().getSiCentral())) {
		re.especificarSonido("sonidos/acertijoPiano/71siCentral.mp3");
	}
if(e.getSource().equals(getVista().getSibCentral())) {
	re.especificarSonido("sonidos/acertijoPiano/70sibCentral.mp3");
	}
if(e.getSource().equals(getVista().getLaCentral())) {
	re.especificarSonido("sonidos/acertijoPiano/69laCentral.mp3");
}
if(e.getSource().equals(getVista().getLabCentral())) {
	re.especificarSonido("sonidos/acertijoPiano/68lavCbentral.mp3");
}
if(e.getSource().equals(getVista().getSolCentral())) {
	re.especificarSonido("sonidos/acertijoPiano/66solbCentral.mp3");
}
if(e.getSource().equals(getVista().getSolbCentral())) {
	re.especificarSonido("sonidos/acertijoPiano/66solbCentral.mp3");
}
if(e.getSource().equals(getVista().getFaCentral())) {
	re.especificarSonido("sonidos/acertijoPiano/65faCentral.mp3");
}
if(e.getSource().equals(getVista().getMiCentral())) {
	re.especificarSonido("sonidos/acertijoPiano/64miCentral.mp3");
	verificador=verificador+"1";
}
if(e.getSource().equals(getVista().getMibCentral())) {
	re.especificarSonido("sonidos/acertijoPiano/63mibCentral.mp3");
	verificador=verificador+"2";
}
if(e.getSource().equals(getVista().getReCentral())) {
	re.especificarSonido("sonidos/acertijoPiano/62reCentral.mp3");
	verificador=verificador+"3";
}
if(e.getSource().equals(getVista().getRebCentral())) {
	re.especificarSonido("sonidos/acertijoPiano/61rebCentral.mp3");
}
if(e.getSource().equals(getVista().getDoCentral())) {
	re.especificarSonido("sonidos/acertijoPiano/60doCentral.mp3");
	verificador=verificador+"5";
}
if(e.getSource().equals(getVista().getSi())) {
	re.especificarSonido("sonidos/acertijoPiano/59si.mp3");
	verificador=verificador+"6";
}
if(e.getSource().equals(getVista().getSib())) {
	re.especificarSonido("sonidos/acertijoPiano/58sib.mp3");
}
if(e.getSource().equals(getVista().getLa())) {
	re.especificarSonido("sonidos/acertijoPiano/57la.mp3");
	verificador=verificador+"8";
}
if(e.getSource().equals(getVista().getLab())) {
	re.especificarSonido("sonidos/acertijoPiano/56lab.mp3");
}
if(e.getSource().equals(getVista().getSol())) {
	re.especificarSonido("sonidos/acertijoPiano/55sol.mp3");
}
if(e.getSource().equals(getVista().getSolb())) {
	re.especificarSonido("sonidos/acertijoPiano/54fab.mp3");
}
if(e.getSource().equals(getVista().getFa())) {
	re.especificarSonido("sonidos/acertijoPiano/53fa.mp3");
}
if(e.getSource().equals(getVista().getMi())) {
	re.especificarSonido("sonidos/acertijoPiano/52mi.mp3");
}
if(e.getSource().equals(getVista().getMib())) {
	re.especificarSonido("sonidos/acertijoPiano/51mib.mp3");
}
if(e.getSource().equals(getVista().getRe())) {
	re.especificarSonido("sonidos/acertijoPiano/50re.mp3");
}
if(e.getSource().equals(getVista().getReb())) {
	re.especificarSonido("sonidos/acertijoPiano/49reb.mp3");
}
if(e.getSource().equals(getVista().getDo())) {
	re.especificarSonido("sonidos/acertijoPiano/48do.mp3");
}

cont=cont+1;
if (cont==7) {
	if (verificador.equals("1216358")) {
		this.rta=true;
		try {
			Thread.sleep(300);
		} catch (InterruptedException e1) {
			
			e1.printStackTrace();
		}
	}
	this.vista.setVisible(false);
}



}




public Boolean getRta() {
	return rta;
}

public void setRta(Boolean rta) {
	this.rta = rta;
}


}
