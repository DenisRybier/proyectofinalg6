package controlador;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.swing.JOptionPane;

import javazoom.jlgui.basicplayer.BasicPlayer;
import javazoom.jlgui.basicplayer.BasicPlayerException;
import modelo.Assets;
import modelo.Boomer;
import modelo.BossFinal;
import modelo.Disparo;
import modelo.DisparosBoss;
import modelo.Ente;
import modelo.ExcepcionNombre;
import modelo.Maquina;
import modelo.NenaRoja;
import modelo.Npc;
import modelo.Lloron;
import modelo.Personaje;
import modelo.Posicion;
import modelo.Ranking;
import modelo.Reproductor;
import modelo.ResumenDao;
import modelo.Runner;
import modelo.Spider;
import vista.VistaJuego;

public class ControladorJuego {
	int puntos = 0;
	public static boolean bossmuerto = false;
	public static String dificultad = "FACIL";
	private Integer vidainicialpersonaje = 250;
	private Integer vidainicialenemigo = 150;
	private Integer da�oinicial = 30;
	private Personaje personaje;
	private Disparo disparo;
	private Boomer enemigo, enemigo2;
	private Runner enemigo3;
	private Boolean continuar = true;
	private Lloron lloron, lloron2, lloron3;
	private Maquina maquina;
	public static int mapa = 1;
	private Boolean acertijo = false;
	private Boolean temp1 = false;
	private Boolean temp = false;
	private Integer vidaMax;
	private Integer vida;
	private Integer muni;
	private Integer da�o;
	private Boolean bossSpiderMuerto = false;
	private Boolean vidaExtra = true;
	private Boolean mapaCargado = true;
	private Boolean tieneLlaves = false;
	private ControladorAcertijo controladorAcertijo;
	private Boolean enemigoMuerto = false;
	private Boolean enemigoMuerto2 = false;
	private Boolean enemigoMuerto3 = false;
	private Spider boss;
	private Boolean llaveDisponible = true;
	private Boolean llaveDisponible2 = true;
	private Boolean llaveDisponible3 = true;
	private BossFinal jefe;
	private Boolean DisparaUnaVez = true;
	private Integer contadorLlaves = 0;
	private BufferedImage mapaActual;
	Reproductor re;
	private DisparosBoss disparosBoss;
	BasicPlayer basic = new BasicPlayer();
	int i = 0;
	int j = 0;
	int k = 0;
	int l = 0;
	private int m, s, cs;
	private String tiempo = "";
	private Boolean terminado = false;
	private ResumenDao resumen;
	private Npc npc;
	private NenaRoja nena;
	private Boolean entroCasa = false;
	private Boolean setransformo = false;
	private Boolean entro = false;
	private Boolean dejarDeDibujar = false;
	
	private Boolean DatosIniciados =false;
	
	private Boolean enemigoInstanciado,enemigo2Instanciado,
	enemigo3Instanciado,lloron1Instanciado,
	lloron2Instanciado,lloron3Instanciado,spiderInstanciado,
	jefeFinalInstanciado,maquinaInstanciado,npcInstanciado = false;
	
	private Integer contadorFin=0;
	
	private Boolean npcInstanciadox2 = false;

	public ControladorJuego(String nombreUsuario) {
		this.resumen = new ResumenDao();
		re = new Reproductor();
		estadoInicial();
        contadorFin=0;
		try {

			basic.open(new File("sonidos/ambientacion.mp3"));
			basic.play();
		} catch (BasicPlayerException e) {
			e.printStackTrace();
		}
		personaje = new Personaje(new Posicion(832, 344), Assets.personaje, 250, vidainicialpersonaje, 10, da�oinicial,
				true, 3, Assets.zona1, null);

		personaje.setNombre(nombreUsuario);

        mapa=1;
        
		if (mapa == 1) {

			disparo = new Disparo(new Posicion(1000, 16), Assets.bala, 50, 50, 0, Assets.zona1); // VIDAMAX,VIDA,DA�O

			enemigo = new Boomer(new Posicion(900, 200), Assets.boom, 150, vidainicialenemigo, 250, Assets.zona1);
			enemigo2 = new Boomer(new Posicion(1050, 300), Assets.boom, 150, vidainicialenemigo, 250, Assets.zona1);
			enemigo3 = new Runner(new Posicion(550, 350), Assets.runner, 200, vidainicialenemigo, 100, Assets.zona1);
			
			
            enemigoInstanciado=true;
            enemigo2Instanciado=true;
            enemigo3Instanciado=true;
			spiderInstanciado=false;
			jefeFinalInstanciado=false;
			lloron1Instanciado=false;
			lloron2Instanciado=false;
			lloron3Instanciado=false;
			npcInstanciado=false;
			npcInstanciadox2=false;
			maquinaInstanciado=false;
			
		}

	}

	public void update() {

		if (Personaje.enpausa == false) {

			if (!(terminado)) {
				cs++;
				cs++;
				cs++;

				if (cs >= 100) {
					cs = 0;
					++s;
				}
				if (s >= 60) {
					s = 0;
					++m;
				}
				if (m >= 60) {
					m = 0;
				}
				actualizarTiempo();
			}
		}

		personaje.setNumMapa(mapa);

		if (continuar) {
			personaje.update();
		}
		// si el personaje dispara Y la bala est� en el "aire" se har� lo siguiente
		if (personaje.isElDispara() && disparo.isVivo() && personaje.isTieneMunicion()) {
			disparo.setImagen(Assets.bala);

			if (DisparaUnaVez) {
				disparo.seguirPersonaje(personaje.obtenerX(), personaje.obtenerY());
				DisparaUnaVez = false;
			}

			continuar = false;
			disparo.obtenerDireccion(personaje.getDireccionDisparo());
			disparo.update();

		} else {
			DisparaUnaVez = true;
			disparo.setVivo(true);
			personaje.setElDispara(false);
			continuar = true;

		}
		if (mapa == 1) {

			da�oRecibido(personaje, enemigo);
			enemigo.update();
			da�o(enemigo);

			if (enemigo.muerto != 1) {
				seguimiento(enemigo);
			} else {
				enemigoMuerto = true;
			}

			da�oRecibido(personaje, enemigo2);
			enemigo2.update();
			da�o(enemigo2);

			if (enemigo2.muerto != 1) {
				seguimiento(enemigo2);
			} else {
				enemigoMuerto2 = true;
			}

			da�oRecibido(personaje, enemigo3);
			enemigo3.update();
			da�o(enemigo3);
			seguimiento(enemigo3);

		}
		if (mapa == 2) {
			da�oRecibido(personaje, boss);
			boss.update();
			da�o(boss);
			if (boss.muerto != 1) {
				seguimiento(boss);
			} else {
				bossSpiderMuerto = true;
			}

		}

		if (mapa == 3) {
			da�oRecibido(personaje, lloron);
			lloron.update();
			da�o(lloron);
			seguimiento(lloron);
			da�oRecibido(personaje, lloron2);
			lloron2.update();
			da�o(lloron2);
			seguimiento(lloron2);
			da�oRecibido(personaje, lloron3);
			lloron3.update();
			da�o(lloron3);
			seguimiento(lloron3);

			da�oRecibido(personaje, maquina);
			maquina.update();
			da�o(maquina);

			da�oRecibido(personaje, jefe);
			jefe.update();
			if (personaje.getBoundsAbajo().intersects(jefe.getRangoAtaque())) {
				jefe.setNenaviva(false);
			}
			da�o(jefe);

			// ataque update
			if (jefe.getComienzaataque() == true && disparosBoss.isVivo()) {
				disparosBoss.update();

			}

			if (personaje.getBoundsAbajo().intersects(disparosBoss.boundDisparo())) {
				personaje.setVida(personaje.getVida() - 130);
			}
		}
		if (mapa == 4) {

			npc.update();
			
			if(npcInstanciadox2) {
			da�o(npc);
			}
			if (entro) {
				da�oRecibido(personaje, npc);
			}
			if (setransformo) {
				npc.setDa�o(100);
				seguimiento(npc);
			}
		}
		if (mapa == 5) {
			nena.update();

			if (npc.getVida() <= 0) {

				da�oRecibido(personaje, nena);
				da�o(nena);
				seguimiento(nena);

			}
			if (entro) {
				seguimiento(npc);
			}
		}
		
		if(!DatosIniciados) {
			switchDificultad();
			DatosIniciados=true;
			}


	}

	public void draw(Graphics g) {

		//re = new Reproductor();
		if (Personaje.enpausa == false) {
            if(mapa == 1) {
            	
			if (personaje.getBoundsArriba().intersects(casa()) && tieneLlaves) {
				entroCasa = true;
				personaje.setRuta(Assets.zona4);
				personaje.setPosicion(new Posicion(790, 500));
				mapa = 4;
				npcInstanciadox2=false;
					
			}
				
			}
            
			if (mapa == 4) {
				
				if(!npcInstanciadox2) {
					npc = new Npc(new Posicion(830, 280), Assets.abuela, 25, 25, 0, Assets.zona4);
					npcInstanciadox2=true;
				}
				
				if (personaje.isX2() == false) {
					dibujarTexto(g, Assets.textoabuela);
				}

				mapaActual = Assets.mapaCasa;
				if (npc.muerto != 1) {
					npc.draw(g);
					
					//personaje.setX2(true);
					
					if (personaje.getBoundsArriba().intersects(salidaCasa())) {
						mapa = 1;
						personaje.setRuta(Assets.zona1);
						personaje.setPosicion(new Posicion(832, 496));
					}
					if (setransformo) {
						npc.setTransformada(true);
					}
					if (entro) {
						nena.draw(g);
					}

				}
				if (entro && npc.getVida() <= 0) {
					if (terminado == false) {
                        subirABasedeDatos();
					}
					dejarDeDibujar = true;
					try {
						basic.stop();
					} catch (BasicPlayerException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if (k == 0) {
						re.especificarSonido("sonidos/Abuse-In-The-Orphanage.mp3");
						k++;
					}
					animarOscuro(g);
					if(contadorFin==700) {
                        if(dejarDeDibujar) {
						re.detenerSonido();
                        }
						VistaJuego.juegoTerminado=true;
					}
				}

			}

			if (!continuar) {
				disparo.draw(g);
			}

			if (!dejarDeDibujar) {
				personaje.draw(g);
			}

			if (mapa == 1) {
				mapaActual = Assets.mapa1;
				npcInstanciadox2=false;
				if (enemigo.muerto != 1) {
					enemigo.draw(g);

				} else {
					if (i == 0) {
						i++;
						re.especificarSonido("sonidos/death.mp3");
					}
					recogerLlave1(g);

				}
				if (enemigo2.muerto != 1) {
					enemigo2.draw(g);
				} else {
					recogerLlave2(g);
					if (j == 0) {

						re.especificarSonido("sonidos/death.mp3");
						j++;
					}
				}
				if (enemigo3.muerto != 1) {
					enemigo3.draw(g);
				} else {
					recogerLlave3(g);

				}

				if (contadorLlaves >= 2) {
					tieneLlaves = true;
					g.drawImage(Assets.flecha_hacia_abajo, 832, 440, null);
					g.drawImage(Assets.flecha_hacia_abajo, 290, 500, null);
				}
				if (personaje.getBoundsArriba().intersects(entradaMuseo()) && tieneLlaves && entroCasa) {
					try {
						if (mapaCargado) {
							Thread.sleep(1000);
							mapaCargado = false;
						}
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					personaje.setRuta(Assets.zona2);
					boss = new Spider(new Posicion(750, 350), Assets.spider1, 500, 500, 100, Assets.zona2);
					mapa = mapa + 1;
					disparo.setRuta(Assets.zona2);
				}
			}

			if (mapa == 2) {

				mapaActual = Assets.mapaMuseo;

				if (boss.muerto != 1) {
					boss.draw(g);
				} else {
					recogerVidaExtra(g);
				}

				if (personaje.getBoundsArriba().intersects(piano())) {

					if (!temp1) {

						JOptionPane.showMessageDialog(null, "Mi=1, 1216358, Am, F�r Therese");
						temp1 = true;
					}
					if (!acertijo) {

						controladorAcertijo = new ControladorAcertijo();

						acertijo = true;
					}

					if (controladorAcertijo.getRta() == true && !temp) {
						personaje.setVida(1000);
						temp = true;
						JOptionPane.showMessageDialog(null, "Logrado");
					}

				}
				if (personaje.getBoundsArriba().intersects(cueva()) && boss.getVida() <= 0) {
					personaje.setRuta(Assets.zona3);
					personaje.setPosicion(new Posicion(768.5, 590));
					lloron = new Lloron(new Posicion(900, 900), Assets.lloron, 500, 500, 30, Assets.zona3);
					lloron2 = new Lloron(new Posicion(500, 208), Assets.lloron, 500, 500, 30, Assets.zona3);
					lloron3 = new Lloron(new Posicion(300, 400), Assets.lloron, 500, 500, 30, Assets.zona3);

					maquina = new Maquina(new Posicion(528, 16), Assets.maquina1, 2000, 2000, 30, Assets.zona3);
					jefe = new BossFinal(new Posicion(912, 520), Assets.azul, 5000, 5000, 30, Assets.zona3);
					// ataque=new AtaqueBoss(new Posicion(1100, 604), Assets.ataqueBoss, 0, 0, 50,
					// null);
					disparosBoss = new DisparosBoss(new Posicion(1100, 604), Assets.ataqueBoss);
					disparo.setRuta(Assets.zona3);
					mapa = mapa + 1;

				} else if (personaje.getBoundsAbajo().intersects(salidaMuseo())) {

					personaje.setRuta(Assets.zona1);
					mapa = 1;
				}
			}
			if (mapa == 3) {
				if (personaje.getX() == false) {
					dibujarTexto(g, Assets.textoazul);
				}
				// ataque random entre 800 y 1200 en X draw
				if (jefe.getComienzaataque() == true && jefe.muerto != 1) {
					int r = (int) (Math.random() * 1100) + 810;

					disparosBoss.draw(g);

					if (!disparosBoss.isVivo()) {
						disparosBoss.setPosicion(new Posicion(r, 604));
						disparosBoss.setVivo(true);
					}

				}

				mapaActual = Assets.mapaTunel;

				if (lloron.muerto != 1) {
					lloron.draw(g);
				}
				if (lloron2.muerto != 1) {
					lloron2.draw(g);
				}
				if (lloron3.muerto != 1) {
					lloron3.draw(g);
				}
				if (jefe.muerto != 1) {
					jefe.draw(g);
				} else {
					if (!personaje.isX3()) {
						bossmuerto = true;
						dibujarTexto(g, Assets.textoboss);
					}
					g.drawImage(Assets.flecha_hacia_abajo, 1140, 190, null);

				}
				if (maquina.muerto != 1) {
					maquina.draw(g);
				}

				if (personaje.getBoundsAbajo().intersects(salidaTunel())) {
					System.out.println("salida tunel");
					personaje.setRuta(Assets.zona2);
					mapa = 2;
				}

				if (personaje.getBoundsDerecha().intersects(salidaTunel2(g))) {
					personaje.setRuta(Assets.zona5);
					mapa = 5;
					personaje.setPosicion(new Posicion(600, 628));

					nena = new NenaRoja(new Posicion(580, 500), Assets.roja, 100, 100, 10, Assets.zona3);
				}

			}

			if (mapa == 5) {
				mapaActual = Assets.mapaFaro;
				if (nena.getVida() > 0 && npc.getVida() <= 0) {
					g.drawImage(Assets.cuchillo, (int) nena.getPosicion().getX() + 48,
							(int) nena.getPosicion().getY() + 32, null);
				}
				nena.draw(g);
				if (personaje.getBoundsAbajo().intersects(salidaFaro())) {
					setransformo = true;
					personaje.setPosicion(new Posicion(754, 512));
					mapa = 4;
					entro = true;
					personaje.setRuta(Assets.zona4);
					nena.setPosicion(new Posicion(700, 480));
				}

				if (npc.getVida() <= 0 && personaje.getBoundsArriba().intersects(final1())) {

					if (terminado == false) {
                        subirABasedeDatos();
					}
					
					animarFaro(g);
					dejarDeDibujar = true;
					if (k == 0) {
						try {
							basic.stop();
						} catch (BasicPlayerException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						re.especificarSonido("sonidos/BetterOfAlone-piano.mp3");
						
						g.setColor(Color.BLACK);
						g.setFont(new Font("Verdana", 1, 23));
                        g.drawString("EL F�N, HAS GANADO", 0, 0);
                        
						if(contadorFin==700) {
	                        if(dejarDeDibujar) {
							re.detenerSonido();
	                        }
							VistaJuego.juegoTerminado=true;
						}
					}
					k++;
				}

			}

			if (!dejarDeDibujar) {
				g.setColor(Color.WHITE);
				g.drawString("Tiempo = " + tiempo, 110, 109);
			}

		} else {
			g.setColor(Color.BLACK);
			g.fillRect(0, 0, 1200, 720);
			g.setColor(Color.WHITE);
			g.setFont(new Font("Verdana", 1, 23));
			g.drawString("PAUSA", 580, 340);
		}

	}

	///// ----------------CONTROLAR DIFICULTAD-------------------------
	public void estadoInicial() {
		switch (dificultad) {
		case "FACIL":
			setVidainicialpersonaje(500);
			setVidainicialenemigo(100);
			setDa�oinicial(200);
			break;
		case "MEDIA":
			setVidainicialpersonaje(450);
			setVidainicialenemigo(200);
			setDa�oinicial(100);
			break;
		case "DIFICIL":
			setVidainicialpersonaje(350);
			setVidainicialenemigo(300);
			setDa�oinicial(50);
			break;
		}
	}

	public void switchDificultad() {
		switch (dificultad) {
		case "FACIL":
			setDificultad(100, 100, 500);
			break;
		case "MEDIA":
			setDificultad(200, 200, 450);
			break;
		case "DIFICIL":
			setDificultad(300, 300, 300);
			break;
		}
	}

public void setDificultad(Integer vida, Integer da�o, Integer vidapersonaje) {
		
		if(enemigoInstanciado) {
		enemigo.setVidamax(vida);
		enemigo.setDa�o(da�o);
		}
		
		if(enemigo2Instanciado) {
		enemigo2.setVidamax(vida);
		enemigo2.setDa�o(da�o);
		}
		
		if(enemigo3Instanciado) {
		enemigo3.setVidamax(vida);
		enemigo3.setDa�o(da�o);
		}
		
		if(spiderInstanciado) {
			boss.setVidamax(vida);
			boss.setDa�o(vida);
		}
		
		if(jefeFinalInstanciado) {
		jefe.setVidamax(vida);
		jefe.setDa�o(vida);
		}
		
		if(npcInstanciado) {
		npc.setVidamax(vida);
		npc.setDa�o(vida);
		}
		
		if(lloron1Instanciado) {
		lloron.setVidamax(vida);
		lloron.setDa�o(vida);
		}
		
		if(lloron2Instanciado) {
			lloron2.setVidamax(vida);
			lloron2.setDa�o(da�o);
		}
		
		if(lloron3Instanciado) {
			lloron3.setVidamax(vida);
			lloron3.setDa�o(da�o);
		}
		
		if(maquinaInstanciado) {
		maquina.setVidamax(vida);
		maquina.setDa�o(vida);
		}
		
			
		personaje.setVidamax(vidapersonaje);
	}

///// ------------------------------- CONTORLAR SEGUIMIENTO ENEMIGOS --------------------

	public void seguimiento(Ente enemigo) {
		if (enemigo.getRangoAtaque().intersects(personaje.boundVista())) {
			if (!(enemigo.colisionesObjetos() == 1) && !(enemigo.colisionesObjetos() == 2)
					&& !(enemigo.colisionesObjetos() == 3) && !(enemigo.colisionesObjetos() == 4)
					|| enemigo instanceof Spider) {
				if (enemigo.getPosicion().getX() < personaje.getPosicion().getX()) {
					enemigo.getPosicion().setX(enemigo.getPosicion().getX() + 1);
				}
				if (enemigo.getPosicion().getX() > personaje.getPosicion().getX()) {
					enemigo.getPosicion().setX(enemigo.getPosicion().getX() - 1);
				}
				if (enemigo.getPosicion().getY() < personaje.getPosicion().getY()) {
					enemigo.getPosicion().setY(enemigo.getPosicion().getY() + 1);
				}
				if (enemigo.getPosicion().getY() > personaje.getPosicion().getY()) {
					enemigo.getPosicion().setY(enemigo.getPosicion().getY() - 1);
				}
			} else {
				correguirRuta(enemigo);
			}
		}
	}

	public void da�o(Ente enemigo) {
		if (disparo.BoundBala().intersects(enemigo.getBoundEnemy()) && !(enemigo instanceof Npc)) {
			enemigo.setVida(enemigo.getVida() - personaje.getDa�o());
			personaje.aumentarExp();
		}
		
		if (disparo.BoundBala().intersects(enemigo.getBoundEnemy()) && (enemigo instanceof Npc)) {
			enemigo.setVida(enemigo.getVida() - personaje.getDa�o());
		}
	}

	public void da�oRecibido(Personaje personaje, Ente ente) {
		re = new Reproductor();
		if (personaje.getBoundsAbajo().intersects(ente.getBoundEnemy())
				|| personaje.getBoundsArriba().intersects(ente.getBoundEnemy())
				|| personaje.getBoundsDerecha().intersects(ente.getBoundEnemy())
				|| personaje.getBoundsIzquierda().intersects(ente.getBoundEnemy())) {
			personaje.setVida(personaje.getVida() - ente.getDa�o());
			re.especificarSonido("sonidos/playerhurt.mp3");

			if (ente instanceof Boomer) {

				ente.setVida(0);
			}
			if (ente instanceof Runner) {
				((Runner) ente).setAtaca(true);

			}

		}

	}

	public Disparo getDisparo() {
		return disparo;
	}

	public void setDisparo(Disparo disparo) {
		this.disparo = disparo;
	}

	public void correguirRuta(Ente ente) {
		if (ente.colisionesObjetos() == 1) {
			ente.getPosicion().setX(ente.getPosicion().getX() + 2);
		}
		if (ente.colisionesObjetos() == 2) {
			ente.getPosicion().setX(ente.getPosicion().getX() - 2);
		}
		if (ente.colisionesObjetos() == 3) {
			ente.getPosicion().setY(ente.getPosicion().getY() - 2);
		}
		if (ente.colisionesObjetos() == 4) {
			ente.getPosicion().setY(ente.getPosicion().getY() + 2);
		}
	}

	public Rectangle entradaMuseo() {
		return new Rectangle(298, 632, 8, 8);
	}

	public Integer getVidaMax() {
		return vidaMax;
	}

	public Integer getVida() {
		return vida;
	}

	public Integer getMuni() {
		return muni;
	}

	public Integer getDa�o() {
		return da�o;
	}

	public void recogerLlave1(Graphics g) {

		if (enemigoMuerto && llaveDisponible) {
			dibujarLlave(g, enemigo.getPosicion().getX(), enemigo.getPosicion().getY());
		}

		if (personaje.getBoundsArriba().intersects(
				dibujarRecLlave(enemigo.getPosicion().getX(), enemigo.getPosicion().getY())) && llaveDisponible) {
			re.especificarSonido("sonidos/pickup.mp3");
			llaveDisponible = false;
			contadorLlaves++;
		}

	}

	public String valoracion() {

		puntos = puntos + personaje.getVidas() * 100;

		puntos = puntos - (m * 50) - (s * 2) + personaje.getVida();
		if (puntos < 100.0) {
			return "D";
		}
		if (puntos > 100.0 && puntos < 200.0) {
			return "C";
		}
		if (puntos > 200.0 && puntos < 300.0) {
			return "B";
		} else {
			return "A";
		}

	}

	public void recogerLlave2(Graphics g) {
		re = new Reproductor();
		if (enemigoMuerto2 && llaveDisponible2) {
			dibujarLlave(g, enemigo2.getPosicion().getX(), enemigo2.getPosicion().getY());
		}

		if (personaje.getBoundsArriba().intersects(
				dibujarRecLlave(enemigo2.getPosicion().getX(), enemigo2.getPosicion().getY())) && llaveDisponible2) {

			re.especificarSonido("sonidos/pickup.mp3");

			llaveDisponible2 = false;
			contadorLlaves++;
		}

	}

	public void recogerLlave3(Graphics g) {

		if (enemigoMuerto3 && llaveDisponible3) {
			dibujarLlave(g, enemigo3.getPosicion().getX(), enemigo3.getPosicion().getY());
		}

		if (personaje.getBoundsArriba().intersects(
				dibujarRecLlave(enemigo3.getPosicion().getX(), enemigo3.getPosicion().getY())) && llaveDisponible3) {
			re.especificarSonido("sonidos/pickup.mp3");
			llaveDisponible3 = false;
			contadorLlaves++;
		}

	}

	public void dibujarLlave(Graphics g, double posicionXEnemigo, double posicionYEnemigo) {
		g.drawImage(Assets.llave, (int) posicionXEnemigo, (int) posicionYEnemigo, null);
	}

	public void dibujarTexto(Graphics g, BufferedImage imagen) {
		g.drawImage(imagen, 0, 100, null);
	}

	public Rectangle dibujarRecLlave(double posicionXEnemigo, double posicionYEnemigo) {
		return new Rectangle((int) posicionXEnemigo, (int) posicionYEnemigo, 10, 10);
	}

	public Rectangle piano() {
		if (mapa == 2) {
			return new Rectangle(105, 156, 5, 5);
		}
		return null;
	}

	public Rectangle cueva() {
		if (mapa == 2) {

			return new Rectangle(487, 236, 5, 5);
		}
		return null;
	}

	public Rectangle dibujarVidaExtra(Graphics g) {
		g.drawImage(Assets.health, 488, 432, null);
		return new Rectangle((int) 488, 432, 10, 10);
	}

	public void recogerVidaExtra(Graphics g) {
		if (bossSpiderMuerto && vidaExtra && personaje.getBoundsArriba().intersects(dibujarVidaExtra(g))) {
			dibujarVidaExtra(g);
			re.especificarSonido("/sonidos/pickup.mp3");
			vidaExtra = false;
			personaje.setVidas(personaje.getVidas() + 1);
		}

	}

	public Rectangle salidaMuseo() {
		if (mapa == 2) {
			return new Rectangle(489, 650, 10, 10);
		}
		return fuera();
	}

	public Rectangle salidaTunel() {
		if (mapa == 3) {
			return new Rectangle((int) 768.5, 646, 10, 10);
		}
		return fuera();
	}

	public Rectangle casa() {
		if (mapa == 1 && !entro) {
			return new Rectangle(832, 484, 5, 5);
		} else {
			return fuera();
		}
	}

	public Rectangle salidaCasa() {
		if (mapa == 4 && entro == false) {

			return new Rectangle(754, 512, 64, 64);
		}
		return fuera();
	}

	public Rectangle salidaFaro() {
		if (mapa == 5 && npc.getVida() > 0) {

			return new Rectangle(1040, 668, 64, 64);
		} else

			return fuera();
	}

	public Rectangle final1() {
		if (mapa == 5 && npc.getVida() <= 0) {
			return new Rectangle(1040, 460, 64, 64);
		} else {
			return fuera();
		}
	}

	public Rectangle salidaTunel2(Graphics g) {
		if (mapa == 3 && boss.muerto == 1) {

			return new Rectangle((int) 1164, 206, 32, 32);
		}
		return fuera();
	}

	public BufferedImage getMapaActual() {
		return mapaActual;
	}

	public Rectangle fuera() {
		return new Rectangle(1000, 1000, 32, 32);
	}

	public void setMapaActual(BufferedImage mapaActual) {
		this.mapaActual = mapaActual;
	}

	public Personaje getPersonaje() {
		return personaje;
	}

	public void setPersonaje(Personaje personaje) {
		this.personaje = personaje;
	}

	public Integer getVidainicialpersonaje() {
		return vidainicialpersonaje;
	}

	public void setVidainicialpersonaje(Integer vidainicialpersonaje) {
		this.vidainicialpersonaje = vidainicialpersonaje;
	}

	public Integer getVidainicialenemigo() {
		return vidainicialenemigo;
	}

	public void setVidainicialenemigo(Integer vidainicialenemigo) {
		this.vidainicialenemigo = vidainicialenemigo;
	}

	public Integer getDa�oinicial() {
		return da�oinicial;
	}

	public void setDa�oinicial(Integer da�oinicial) {
		this.da�oinicial = da�oinicial;
	}

	private void actualizarTiempo() {
		tiempo = ((m <= 9 ? "0" : "") + m + ":" + (s <= 9 ? "0" : "") + s + "." + (cs <= 9 ? "0" : "") + cs);
	}

	public ResumenDao getResumen() {
		return resumen;
	}

	public void setResumen(ResumenDao resumen) {
		this.resumen = resumen;
	}

	// falta controlar los espacios en blanco
	public void validarNombre(String nombre) throws ExcepcionNombre {

		if (nombre.matches("[a-zA-Z]*")) {
			personaje.setNombre(nombre);
		} else {
			throw new ExcepcionNombre();
		}

	}

	int animacionFaro = 0;

	public void animarEscena() {
		if (animacionFaro < 32767) {
			animacionFaro++;
		} else {
			animacionFaro = 0;
		}
	}

	public void animarFaro(Graphics g) {
		animarEscena();
		int resto = animacionFaro % 30;

		if (resto > 10 && resto <= 20) {
			g.drawImage(Assets.animacionfaro1, 0, 0, null);
		} else if (resto > 20 && resto <= 30) {
			g.drawImage(Assets.animacionfaro2, 0, 0, null);
		} else {
			g.drawImage(Assets.animacionfaro3, 0, 0, null);

		}
	}

	public void animarOscuro(Graphics g) {
		animarEscena();
		int resto = animacionFaro % 50;
		if (l < 60) {
			if (resto > 5 && resto <= 10) {
				g.drawImage(Assets.animoscuro1, 0, 0, null);
			} else if (resto > 10 && resto <= 15) {
				g.drawImage(Assets.animoscuro2, 0, 0, null);
			} else if (resto > 15 && resto <= 20) {
				g.drawImage(Assets.animoscuro3, 0, 0, null);
			} else if (resto > 20 && resto <= 25) {
				g.drawImage(Assets.animoscuro4, 0, 0, null);
			} else if (resto > 25 && resto <= 30) {
				g.drawImage(Assets.animoscuro5, 0, 0, null);
			} else if (resto > 35 && resto <= 35) {
				g.drawImage(Assets.animoscuro6, 0, 0, null);
			} else if (resto > 40 && resto <= 40) {
				g.drawImage(Assets.animoscuro7, 0, 0, null);
			} else if (resto > 40 && resto <= 45) {
				g.drawImage(Assets.animoscuro8, 0, 0, null);
			} else if (resto > 45 && resto <= 50) {
				g.drawImage(Assets.animoscuro9, 0, 0, null);
			} else {
				g.drawImage(Assets.oscuro, 0, 0, null);
			}

			l++;
		} else {
			g.drawImage(Assets.oscuro10, 0, 0, null);
		}
	}
	
	public void subirABasedeDatos() {
		terminado = true;
		/// Guardar en tabla los resultados

		System.out.println("entro");
		resumen = new ResumenDao();
		System.out.println("lo pase");
		if (getResumen().maxId() != null) {

			System.out.println("entre");
			int id = getResumen().maxId() + 1;
			Ranking ranking = new Ranking(id, personaje.getNombre(), tiempo,
					String.valueOf(personaje.getVidas()), valoracion(), String.valueOf(puntos));
			getResumen().save(ranking);
		} else {
			System.out.println("maxIdnulo");
		}
	}
}
