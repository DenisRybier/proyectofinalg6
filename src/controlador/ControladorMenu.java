package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import javazoom.jlgui.basicplayer.BasicPlayer;
import javazoom.jlgui.basicplayer.BasicPlayerException;
import modelo.ExcepcionNombre;
import modelo.Ranking;
import modelo.Reproductor;
import modelo.ResumenDao;
import vista.VistaJuego;
import vista.VistaMenu;

public class ControladorMenu implements ActionListener {
	
	VistaMenu vistamenu;
	Boolean musicasigue = true;
	BasicPlayer basic = new BasicPlayer();
	Boolean nombreValidado = false;
	
	public ControladorMenu() {

		this.vistamenu = new VistaMenu(this);
		this.vistamenu.setVisible(true);
		try {

			basic.open(new File("sonidos/musicaMenu.mp3"));
			basic.play();
		} catch (BasicPlayerException g) {
			g.printStackTrace();
		}
		
		actualizarTable();

	}

	public void transicionOpciones(Boolean valoranterior, Boolean valorproximo) {
		vistamenu.getBtnEmpezar().setVisible(valoranterior);
		vistamenu.getBtnOpciones().setVisible(valoranterior);
		vistamenu.getBtnRanking().setVisible(valoranterior);
		vistamenu.getBtnSalir().setVisible(valoranterior);
		estadoDificultad(valorproximo);

	}

	public void estadoDificultad(Boolean valor) {
		vistamenu.getBtnDificultad().setVisible(valor);
		vistamenu.getBtnGuia().setVisible(valor);
		vistamenu.getBtnVolver().setVisible(valor);
		vistamenu.getBtnVolver().setBounds(270, 250, 120, 40);
	}

	public void transicionDificultad(Boolean valoranterior, Boolean valorproximo) {
		estadoDificultad(valoranterior);
		vistamenu.getBtnDificil().setVisible(valorproximo);
		vistamenu.getBtnMedia().setVisible(valorproximo);
		vistamenu.getBtnFacil().setVisible(valorproximo);
	}

	public void ocultarTablas() {
		vistamenu.getLabelguia().setVisible(false);
		vistamenu.getScrollpane().setVisible(false);
		vistamenu.getTable().setVisible(false);
		vistamenu.getScrollpane2().setVisible(false);
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		JButton btn = (JButton) e.getSource();

		switch (btn.getText()) {
		case "EMPEZAR":
			try {
				basic.stop();
			} catch (BasicPlayerException e1) {
				e1.printStackTrace();
			}		
			
			while (!nombreValidado) {
				String nombre = (JOptionPane.showInputDialog("Escriba su nombre de usuario (sin espacios) para jugar"));
				try {				
					validarNombre(nombre);
					vistamenu.setVisible(false);
				} catch (ExcepcionNombre e2) {
					JOptionPane.showMessageDialog(null, e2.getMessage());
					nombreValidado=false;
				} catch (NullPointerException e3) {
					nombreValidado=true;					
				}
			}
			
			if(nombreValidado) {
				nombreValidado=false;
			}
			
			setMusicasigue(false);

			break;
		case "RANKING":
			// VISTA
			transicionOpciones(false, false);
			vistamenu.getTable().setVisible(true);
			vistamenu.getScrollpane2().setVisible(true);
			vistamenu.getBtnVolver().setVisible(true);
			vistamenu.getBtnVolver().setBounds(270, 398, 120, 40);

			// LISTA

			break;
		case "SALIR":
			System.exit(0);
			break;
		case "OPCIONES":
			transicionOpciones(false, true);
			break;
		case "DIFICULTAD":
			transicionDificultad(false, true);
			break;
		case "GUIA":
			vistamenu.getLabelguia().setVisible(true);
			vistamenu.getScrollpane().setVisible(true);
			vistamenu.getBtnDificultad().setVisible(false);
			vistamenu.getBtnGuia().setVisible(false);
			vistamenu.getBtnVolver().setBounds(270, 398, 120, 40);
			break;
		case "VOLVER":
			transicionOpciones(true, false);
			ocultarTablas();
			break;
		default:
			ControladorJuego.dificultad = btn.getText();
			switch (btn.getText()) {
			case "DIFICIL":
				transicionDificultad(true, false);
				break;
			case "MEDIA":
				transicionDificultad(true, false);
				break;
			case "FACIL":
				transicionDificultad(true, false);
				break;
			}
		}
	}

	public void actualizarTable() {
		ResumenDao resumen = new ResumenDao();
		List<Ranking> rankings = new ArrayList<Ranking>();
		rankings = resumen.getAll();
		for (int i = 0; i < rankings.size(); i++) {
			Object[] row = { String.valueOf(rankings.get(i).getId()), rankings.get(i).getUsuario(),
					rankings.get(i).getTiempo(), rankings.get(i).getVida(), rankings.get(i).getValoracion(),
					(rankings.get(i).getPuntos()) };
			vistamenu.getModeloTabla().addRow(row);
		}

		//vistamenu.rellenarTabla();
		
	}

	public VistaMenu getVistamenu() {
		return vistamenu;
	}

	public void setVistamenu(VistaMenu vistamenu) {
		this.vistamenu = vistamenu;
	}

	public Boolean getMusicasigue() {
		return musicasigue;
	}

	public void setMusicasigue(Boolean musicasigue) {
		this.musicasigue = musicasigue;
	}

	public void validarNombre(String nombre) throws ExcepcionNombre {

		if (nombre.matches("[a-zA-Z]*[0-9]*")) {
			nombreValidado = true;
			new VistaJuego(nombre).start();
		} else {
			throw new ExcepcionNombre();
		}
		
	}
	
}
