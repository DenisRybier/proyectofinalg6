package controlador;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;



public class Teclado implements KeyListener {
	
	private boolean[] keys = new boolean[256];	
	public static boolean UP, LEFT, DOWN, RIGHT,E,P,X;
    private boolean enMovimiento = false;
	
	public Teclado() {
		UP = false;
		LEFT = false;
		DOWN = false;
		RIGHT = false;
		E=false;
		P=false;
		X=false;
	}
	
	public void update()  {
		
		UP = keys[KeyEvent.VK_W];
		LEFT = keys[KeyEvent.VK_A];
		DOWN = keys[KeyEvent.VK_S];
		RIGHT = keys[KeyEvent.VK_D];
		E=keys[KeyEvent.VK_E];
		P=keys[KeyEvent.VK_P];
		X=keys[KeyEvent.VK_X];
	}
	@Override
	public void keyPressed(KeyEvent e) {
		keys[e.getKeyCode()] = true;
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		keys[e.getKeyCode()] = false;
	}

	@Override
	public void keyTyped(KeyEvent e) {
		
	}

	public boolean isEnMovimiento() {
		return enMovimiento;
	}

	public void setEnMovimiento(boolean enMovimiento) {
		this.enMovimiento = enMovimiento;
	}
	
	
	
	

}