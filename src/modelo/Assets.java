package modelo;

import java.awt.image.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class Assets {
	
	public static BufferedImage personaje,personaje2,personajeUp,
	personaje3,personajeUp2,personajeUp3,personajeRight,personajeRight2
	,personajeRight3,personajeLeft,personajeLeft2,personajeLeft3
	, personajeDeath1, personajeDeath2, personajeDeath3,
	
	
	// PERSONAJE RESCALADO
	persrescal,persrescal2,persrescal3,
	persrescalup,persrescalup2,persrescalup3,persrescalright,persrescalright2
	,persrescalright3,persrescalleft,persrescalleft2,persrescalleft3,
	persrescaldeath1,persrescaldeath2, persrescaldeath3,
	
	
	
	azul,luz,
	abuela,abuelaF,
	
	meta0,meta1,meta2,meta3,meta4,meta5,meta6,meta7,meta8,
	
	meta9,meta10,meta11,meta12,meta13,meta14,meta15,meta16,meta17,
	
	meta18,meta19,meta20,meta21,meta22,meta23,meta24,meta25,meta26,
	
	meta27,meta28,meta29,meta30,
	
	bossf0,bossf1,bossf2,bossf3, bossm0, bossm1, bossm2, bossm3, bossm4, bossm5, bossm6,
	
	ataqueBoss,
	
	mapa1,mapaTunel,mapaMuseo,mapaCasa,mapaFaro,
	
	boom,boom2,boom3,boom4, boomexp, boomexp2, boomexp3, boomexp4, boomexp5, boomexp6, boomexp7,
	
	runner, runner2, runner3, runner4, runner5, runnerA1,runnerA2,runnerA3,runnerA4,runnerA5,runnerA6,
	
	lloron, lloron1, lloron2, lloron3,roja,
	
	health,bala,gun,llave, flecha_hacia_abajo,
	
	spider1,spider2,spider3,spider4,spiderDeath2,spiderDeath3,spiderDeath4,spiderDeath5,spiderDeath6,
	spiderDeath,
	
	maquina1,maquina2,maquina3,maquina4,maquina5,maquinaf,
	
	textoazul, textoabuela, textoboss,
	
	abuet,abuet2,abuet3,abuet4,abuet5,cuchillo,
	
	animacionfaro1,animacionfaro2,animacionfaro3,oscuro,oscuro10,animoscuro9,animoscuro1,animoscuro2,
	animoscuro3,animoscuro4,animoscuro5,animoscuro6,animoscuro7,animoscuro8,negro;
	
	public static String paso1, paso2, ambiente,disparo,zona1,zona2,zona3,zona4,zona5,acertijo1,
	Do,Reb,Re,Mib,Mi,Fa,Solb,Sol,Lab,La,Sib,Si,DoC,RebC,ReC,MibC,MiC,FaC,SolbC,SolC,LabC,LaC,
	SibC,SiC,musicaMenu;
	
	
	public static void init() {	
		maquina1=CargarImagenes.cargar("/JefetTunel/JefeTunel_0.png");
		maquina2=CargarImagenes.cargar("/JefetTunel/JefeTunel_1.png");
		maquina3=CargarImagenes.cargar("/JefetTunel/JefeTunel_2.png");
		maquina4=CargarImagenes.cargar("/JefetTunel/JefeTunel_3.png");
		maquina5=CargarImagenes.cargar("/JefetTunel/JefeTunel_4.png");
		maquinaf=CargarImagenes.cargar("/JefetTunel/JefeTunel_muerto.png");
		
		boom = CargarImagenes.cargar("/boom/enemigoExplota1.png");
		boom2 = CargarImagenes.cargar("/boom/enemigoExplota2.png");
		boom3 = CargarImagenes.cargar("/boom/enemigoExplota3.png");
		boom4 = CargarImagenes.cargar("/boom/enemigoExplota4.png");
		boomexp = CargarImagenes.cargar("/boom/enemigoExplotando_1.png");
		boomexp2 = CargarImagenes.cargar("/boom/enemigoExplotando_2.png");
		boomexp3 = CargarImagenes.cargar("/boom/enemigoExplotando_3.png");
		boomexp4 = CargarImagenes.cargar("/boom/enemigoExplotando_4.png");
		boomexp5 = CargarImagenes.cargar("/boom/enemigoExplotando_5.png");
		boomexp6 = CargarImagenes.cargar("/boom/enemigoExplotando_6.png");
		boomexp7 = CargarImagenes.cargar("/boom/enemigoExplotando_7.png");
		
		
		runnerA1= CargarImagenes.cargar("/corredor/corredorAtaque_0.png");
		runnerA2=CargarImagenes.cargar("/corredor/corredorAtaque_1.png");
	    runnerA3=CargarImagenes.cargar("/corredor/corredorAtaque_2.png");
	    runnerA4=CargarImagenes.cargar("/corredor/corredorAtaque_3.png");
		runnerA5=CargarImagenes.cargar("/corredor/corredorAtaque_4.png");
		runnerA6=CargarImagenes.cargar("/corredor/corredorAtaque_5.png");
		runner = CargarImagenes.cargar("/corredor/corredor_0.png");
		runner2 = CargarImagenes.cargar("/corredor/corredor_1.png");
		runner3 = CargarImagenes.cargar("/corredor/corredor_2.png");
		runner4 = CargarImagenes.cargar("/corredor/corredor_3.png");
		runner5 = CargarImagenes.cargar("/corredor/corredor_4.png");
		
		lloron= CargarImagenes.cargar("/lloron/lloron_0.png");
		lloron1= CargarImagenes.cargar("/lloron/lloron_1.png");
		lloron2= CargarImagenes.cargar("/lloron/lloron_2.png");
		lloron3= CargarImagenes.cargar("/lloron/lloron_3.png");
		
		
		
        spider1 = CargarImagenes.cargar("/ara�a/ara�aMovimiento (1).png");
        spider2 = CargarImagenes.cargar("/ara�a/ara�aMovimiento (2).png");
        spider3 = CargarImagenes.cargar("/ara�a/ara�aMovimiento (3).png");
        spider4 = CargarImagenes.cargar("/ara�a/ara�aMovimiento (4).png");
        spiderDeath = CargarImagenes.cargar("/ara�a/ara�amuerta total-1.png");
        spiderDeath2 = CargarImagenes.cargar("/ara�a/ara�amuerta total-2.png");
        spiderDeath3 = CargarImagenes.cargar("/ara�a/ara�amuerta total-3.png");
        spiderDeath4 = CargarImagenes.cargar("/ara�a/ara�amuerta total-4.png");
        spiderDeath5 = CargarImagenes.cargar("/ara�a/ara�amuerta total-5.png");
        spiderDeath6 = CargarImagenes.cargar("/ara�a/ara�amuerta total-6.png");
		
		personaje = CargarImagenes.cargar("/Personaje/front/quieto/quieto16.png");
		personaje2 = CargarImagenes.cargar("/Personaje/front/camina/paso1.png");
		personaje3 = CargarImagenes.cargar("/Personaje/front/camina/paso2.png");
		
		personajeUp = CargarImagenes.cargar("/Personaje/back/quieto.png");
		personajeUp2 = CargarImagenes.cargar("/Personaje/back/paso 1.png");
		personajeUp3 = CargarImagenes.cargar("/Personaje/back/paso 2.png");
		

		
		personajeRight = CargarImagenes.cargar("/Personaje/lateral/camina/imagenes/standing.png");
		personajeRight2 = CargarImagenes.cargar("/Personaje/lateral/camina/imagenes/step1 right.png");
		personajeRight3 = CargarImagenes.cargar("/Personaje/lateral/camina/imagenes/step2 right.png");
		
		personajeLeft = CargarImagenes.cargar("/Personaje/lateral/camina/imagenes/standing left.png");
		personajeLeft2 = CargarImagenes.cargar("/Personaje/lateral/camina/imagenes/step1 left.png");
		personajeLeft3 = CargarImagenes.cargar("/Personaje/lateral/camina/imagenes/step2 left.png");
		
		personajeDeath1 =CargarImagenes.cargar("/Personaje/img/img death/muerte_0.png");
		personajeDeath2 =CargarImagenes.cargar("/Personaje/img/img death/muerte_1.png");
		personajeDeath3 =CargarImagenes.cargar("/Personaje/img/img death/muerte_2.png");
		
		
		//PERSONAJE RESCALADO
		
		persrescal = CargarImagenes.cargar("/Personaje/front/quieto/quieto64.png");
		persrescal2 = CargarImagenes.cargar("/Personaje/front/camina/paso1_64.png");
		persrescal3 = CargarImagenes.cargar("/Personaje/front/camina/paso2_64.png");
		
		persrescalup = CargarImagenes.cargar("/Personaje/back/quieto64.png");
		persrescalup2 = CargarImagenes.cargar("/Personaje/back/paso 1_64.png");
		persrescalup3 = CargarImagenes.cargar("/Personaje/back/paso 2_64.png");
		
		persrescalright = CargarImagenes.cargar("/Personaje/lateral/camina/imagenes/standing_64.png");
		persrescalright2 = CargarImagenes.cargar("/Personaje/lateral/camina/imagenes/step1 right_64.png");
		persrescalright3 = CargarImagenes.cargar("/Personaje/lateral/camina/imagenes/step2 right_64.png");
		
		persrescalleft = CargarImagenes.cargar("/Personaje/lateral/camina/imagenes/standing left_64.png");
		persrescalleft2 = CargarImagenes.cargar("/Personaje/lateral/camina/imagenes/step1 left_64.png");
		persrescalleft3 = CargarImagenes.cargar("/Personaje/lateral/camina/imagenes/step2 left_64.png");
		
		persrescaldeath1 =CargarImagenes.cargar("/Personaje/img/img death/muerte_0_64.png");
		persrescaldeath2 =CargarImagenes.cargar("/Personaje/img/img death/muerte_1_64.png");
		persrescaldeath3 =CargarImagenes.cargar("/Personaje/img/img death/muerte_2_64.png");

		
		
		
		// NENAS
		
		roja=CargarImagenes.cargar("/roja/roja64.png");
		cuchillo=CargarImagenes.cargar("/npc/cuchillo.png");
		azul=CargarImagenes.cargar("/azul/azul.png");
		textoazul=CargarImagenes.cargar("/cuadrodetexto/cuadro de texto azul.png");
		textoabuela= CargarImagenes.cargar("/cuadrodetexto/cuadro de texto abu.png");
		textoboss= CargarImagenes.cargar("/cuadrodetexto/cuadro de texto boss.png");

        
		
		
		//ABUELA 		
		abuela=CargarImagenes.cargar("/npc/anciana.png");
		abuelaF=CargarImagenes.cargar("/npc/ancianaMuera.png");
		abuet=CargarImagenes.cargar("/npc/anciana_transformacion0.png");
	    abuet2=CargarImagenes.cargar("/npc/anciana_transformacion1.png");
	    abuet3=CargarImagenes.cargar("/npc/anciana_transformacion2.png");
		abuet4=CargarImagenes.cargar("/npc/anciana_transformacion3.png");
		abuet5=CargarImagenes.cargar("/npc/anciana_transformacion4.png");
		
		meta0=CargarImagenes.cargar("/Transformacion/transformacionazul1.png");
		meta1=CargarImagenes.cargar("/Transformacion/transformacionazul2.png");
		meta2=CargarImagenes.cargar("/Transformacion/transformacionazul3.png");
		meta3=CargarImagenes.cargar("/Transformacion/transformacionazul4.png");
		meta4=CargarImagenes.cargar("/Transformacion/transformacionazul5.png");
		meta5=CargarImagenes.cargar("/Transformacion/transformacionazul6.png");
		meta6=CargarImagenes.cargar("/Transformacion/transformacionazul7.png");
		meta7=CargarImagenes.cargar("/Transformacion/transformacionazul8.png");
		meta8=CargarImagenes.cargar("/Transformacion/transformacionazul9.png");
		meta9=CargarImagenes.cargar("/Transformacion/transformacionazul10.png");
		meta10=CargarImagenes.cargar("/Transformacion/transformacionazul11.png");
		meta11=CargarImagenes.cargar("/Transformacion/transformacionazul12.png");
		meta12=CargarImagenes.cargar("/Transformacion/transformacionazul13.png");
		meta13=CargarImagenes.cargar("/Transformacion/transformacionazul14.png");
		meta14=CargarImagenes.cargar("/Transformacion/transformacionazul15.png");
		meta15=CargarImagenes.cargar("/Transformacion/transformacionazul16.png");
		meta16=CargarImagenes.cargar("/Transformacion/transformacionazul17.png");
		meta17=CargarImagenes.cargar("/Transformacion/transformacionazul18.png");
		meta18=CargarImagenes.cargar("/Transformacion/transformacionazul19.png");
		meta19=CargarImagenes.cargar("/Transformacion/transformacionazul20.png");
		meta20=CargarImagenes.cargar("/Transformacion/transformacionazul21.png");
		meta21=CargarImagenes.cargar("/Transformacion/transformacionazul22.png");
		meta22=CargarImagenes.cargar("/Transformacion/transformacionazul23.png");
		meta23=CargarImagenes.cargar("/Transformacion/transformacionazul24.png");
		meta24=CargarImagenes.cargar("/Transformacion/transformacionazul25.png");
		meta25=CargarImagenes.cargar("/Transformacion/transformacionazul26.png");
		meta26=CargarImagenes.cargar("/Transformacion/transformacionazul27.png");
		meta27=CargarImagenes.cargar("/Transformacion/transformacionazul28.png");
		meta28=CargarImagenes.cargar("/Transformacion/transformacionazul29.png");
		meta29=CargarImagenes.cargar("/Transformacion/transformacionazul30.png");
		meta30=CargarImagenes.cargar("/Transformacion/transformacionazul31.png");
		ataqueBoss=CargarImagenes.cargar("/Transformacion/Ataque.png");
		
		
		
		
		
		
		
		
		
		
		
		
		bossf0 = CargarImagenes.cargar("/Transformacion/BossFInal_0.png");
		bossf1 = CargarImagenes.cargar("/Transformacion/BossFInal_1.png");
		bossf2 = CargarImagenes.cargar("/Transformacion/BossFInal_2.png");
		bossf3 = CargarImagenes.cargar("/Transformacion/BossFInal_3.png");

		
		
		bossm0=CargarImagenes.cargar("/Transformacion/MuerteMeta_0.png");
		bossm1=CargarImagenes.cargar("/Transformacion/MuerteMeta_1.png");
		bossm2=CargarImagenes.cargar("/Transformacion/MuerteMeta_2.png");
		bossm3=CargarImagenes.cargar("/Transformacion/MuerteMeta_3.png");
		bossm4=CargarImagenes.cargar("/Transformacion/MuerteMeta_4.png");
        bossm5=CargarImagenes.cargar("/Transformacion/MuerteMeta_5.png");
        bossm6=CargarImagenes.cargar("/Transformacion/MuerteMeta_6.png");
		
		luz=CargarImagenes.cargar("/recursos_mapa/luz.png");
		mapa1= CargarImagenes.cargar("/Mapa/Mapa.png");
		mapaMuseo=CargarImagenes.cargar("/Mapa/mapamuseo.png");
		mapaTunel=CargarImagenes.cargar("/Mapa/tunel.png");
		mapaCasa=CargarImagenes.cargar("/Mapa/mapaCasa.png");
		mapaFaro=CargarImagenes.cargar("/Mapa/interiorfaro.png");
		bala = CargarImagenes.cargar("/Personaje/objetos/bala.png");
		health = CargarImagenes.cargar("/Personaje/health/health.png");
		gun = CargarImagenes.cargar("/Personaje/gun/gun.png");
		llave= CargarImagenes.cargar("/recursos_mapa/llave.png");
		flecha_hacia_abajo = CargarImagenes.cargar("/recursos_mapa/flecha_hacia_abajo.png");
		oscuro=CargarImagenes.cargar("/fondoMenu/oscuro.png");
		animacionfaro1 = CargarImagenes.cargar("/recursos_mapa/1.png");
		animacionfaro2 = CargarImagenes.cargar("/recursos_mapa/2.png");
		animacionfaro3 = CargarImagenes.cargar("/recursos_mapa/3.png");
		animoscuro1=CargarImagenes.cargar("/fondoMenu/oscuroF00.png");
		animoscuro2=CargarImagenes.cargar("/fondoMenu/oscuroF01.png");
		animoscuro3=CargarImagenes.cargar("/fondoMenu/oscuroF02.png");
		animoscuro4=CargarImagenes.cargar("/fondoMenu/oscuroF03.png");
		animoscuro5=CargarImagenes.cargar("/fondoMenu/oscuroF04.png");
		animoscuro6=CargarImagenes.cargar("/fondoMenu/oscuroF05.png");
		animoscuro7=CargarImagenes.cargar("/fondoMenu/oscuroF06.png");
		animoscuro8=CargarImagenes.cargar("/fondoMenu/oscuroF07.png");
		animoscuro9=CargarImagenes.cargar("/fondoMenu/oscuroF08.png");
		oscuro10=CargarImagenes.cargar("/fondoMenu/oscuroF09.png");
	
		//DEPENDIENDO EL N�MERO QUE VENGA COMO PAR�METRO, VA A REPRODUCIRSE UN SONIDO
		
				/*1 = AMBIENTACI�N PARA EL EXTERIOR
				 *2 = AMBIENTACI�N PARA UN INTERIOR
				 *3 = PODR�A SER UN GOLPE HACIA UN ENEMIGO
				 *4 = MUERTE DE UN JEFE
				 *5 = MUERTE PERSONAJE PRINCIPAL
				 *6 = OTRO GOLPE HACIA UN ENEMIGO
				 *7 = M�SICA PARA EL MEN� PRINCIPAL
				 *8 = RECOGER ALGO
				 *9 = GOLPE HACIA EL PERSONAJE PRINCIPAL
				 *10 = PODR�A SER AL PULSAR UN BOT�N 
				 * 
				 */
		disparo="sonidos/disparo.mp3";
		paso1="sonidos/paso1.mp3";
		paso2="sonidos/paso2.mp3";
		ambiente="sonidos/ambientacion.mp3";
		//sonido piano 
		Do="sonidos/acertijoPiano/48do.mp3";
	    Reb="sonidos/acertijoPiano/49reb.mp3";
	    Re="sonidos/acertijoPiano/50re.mp3";
	    Mib="sonidos/acertijoPiano/51mib.mp3";
	    Mi="sonidos/acertijoPiano/52mi.mp3";
	    Fa="sonidos/acertijoPiano/53fa.mp3";
	    Solb="sonidos/acertijoPiano/54fab.mp3";
	    Sol="sonidos/acertijoPiano/55sol.mp3";
	    Lab="sonidos/acertijoPiano/56lab.mp3";
	    La="sonidos/acertijoPiano/57la.mp3";
	    Sib="sonidos/acertijoPiano/58sib.mp3";
	    Si="sonidos/acertijoPiano/59si.mp3";
	    DoC="sonidos/acertijoPiano/60doCentral.mp3";
	    RebC="sonidos/acertijoPiano/61rebCentral.mp3";
	    ReC="sonidos/acertijoPiano/62reCentral.mp3";
	    MibC="sonidos/acertijoPiano/63mibCentral.mp3";
	    MiC="sonidos/acertijoPiano/64miCentral.mp3";
	    FaC="sonidos/acertijoPiano/65faCentral.mp3";
	    SolbC="sonidos/acertijoPiano/66solbCentral.mp3";
	    SolC="sonidos/acertijoPiano/67solCentral.mp3";
	    LabC="sonidos/acertijoPiano/68lavCbentral.mp3";
	    LaC="sonidos/acertijoPiano/69laCentral.mp3";
		SibC="sonidos/acertijoPiano/70sibCentral.mp3";
		SiC="sonidos/acertijoPiano/71siCentral.mp3";
	    musicaMenu="sonidos/musicaMenu.mp3"	;
		//fin sonido piano
		zona1="recursos/Mapa.json";
		zona2="recursos/mapamuseo.json";
		zona3="recursos/tunel.json";
		zona4="recursos/mapaCasa.json";
		zona5="recursos/faro.json";
	}

}
