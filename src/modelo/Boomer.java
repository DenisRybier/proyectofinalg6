package modelo;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Random;

import controlador.ControladorJuego;

public class Boomer extends Ente {
	Double VEL = 0.5;

	private int r = (int) (Math.random() * 100) + 1;
	public int temp;
	private int movX, movY = 0;

	public Boomer(Posicion posicion, BufferedImage imagen, Integer vidamax, Integer vida, Integer da�o, String ruta) {
		super(posicion, imagen, vidamax, vida, da�o, ruta);
		this.setTemp(temp);
	}

	@Override
	public void update() {

		if (Personaje.enpausa == false) {

			animarEnte();

			animarMuerte();

			if (!(colisionesAtaque() == 1)) {

				dibujarMovimiento();

				movX++;
				if (movX < r) {
					posicion.setX(posicion.getX() - VEL);
				} else {
					movY++;
					if (movY < 20) {
						posicion.setY(posicion.getY() + VEL);
					} else {
						movX = 0;
						movY = 0;
						VEL = -VEL;
					}
				}
			} else {
				if (!(this.temp == 2)) {
					dibujarExplosion();
				}
			}
		}
	}

	@Override
	public void draw(Graphics g) {
		if (!(getImagen() == Assets.boomexp7)) {
			g.drawImage(imagen, (int) posicion.getX(), (int) posicion.getY(), null);
			g.setColor(Color.RED);
			
			g.drawRect((int) posicion.getX() - 2, (int) posicion.getY(),
					getLongitudBarra(getVidamax(), getVida(), 0.16), 1);

		}
	}

	public void dibujarMovimiento() {
		int resto = animacion % 40;

		if (resto > 10 && resto <= 20) {
			setImagen(Assets.boom);
		} else if (resto > 20 && resto <= 30) {
			setImagen(Assets.boom2);
		} else if (resto > 30) {
			setImagen(Assets.boom3);
		} else {
			setImagen(Assets.boom4);
		}
	}

	public void dibujarExplosion() {
		int resto2 = muerte % 70;
		if (resto2 > 10 && resto2 <= 20) {
			setImagen(Assets.boomexp);
		} else if (resto2 > 20 && resto2 <= 30) {
			setImagen(Assets.boomexp2);
		} else if (resto2 > 30 && resto2 <= 40) {
			setImagen(Assets.boomexp3);
		} else if (resto2 > 40 && resto2 <= 50) {
			setImagen(Assets.boomexp4);
		} else if (resto2 > 50 && resto2 <= 60) {
			setImagen(Assets.boomexp5);
		} else if (resto2 > 60 && resto2 <= 70) {
			setImagen(Assets.boomexp6);
		} else {
			setImagen(Assets.boomexp7);
			this.temp = 2;
			muerto = 1;
			System.out.println("murio");
		}
	}

	

	public Rectangle getBoundBoomer() {

		return new Rectangle((int) posicion.getX(), (int) posicion.getY(), 16, 16);
	}

	public int getTemp() {
		return temp;
	}

	public void setTemp(int temp) {
		this.temp = temp;
	}

	@Override
	public Rectangle getBoundEnemy() {
		if (getVida() > 0) {
			return new Rectangle((int) posicion.getX() , (int) posicion.getY()+5, 15, 28);
		} else {
			return new Rectangle(10000, 10000, 32, 32);
		}
	}

}
