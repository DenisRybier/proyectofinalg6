package modelo;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

public class BossFinal extends Ente {
	private Boolean transformado = false;
	private Boolean nenaviva = true;
	private Boolean comienzaataque=false;

	public BossFinal(Posicion posicion, BufferedImage imagen, Integer vidamax, Integer vida, Integer da�o,
			String ruta) {
		super(posicion, imagen, vidamax, vida, da�o, ruta);
	}

	@Override
	public void update() {
		if (Personaje.enpausa == false) {

			animarEnte();
			
			animarMuerte();
			
			if (nenaviva) {
				setImagen(Assets.azul);
			} else {
          
				dibujarTransformacion();
				
                if(transformado) {
                	setComienzaataque(true);
                }
                
				dibujarMovimiento();

				dibujarMuerte();

			}
		}
	}

	@Override
	public void draw(Graphics g) {
		if(muerto != 1) {
		g.drawImage(imagen, (int) posicion.getX(), (int) posicion.getY(), null);
		}
		g.setColor(Color.RED);
		if (!nenaviva && getVida() > 0) {
			g.drawRect((int) posicion.getX() - 2, (int) posicion.getY() - 10,
					getLongitudBarra(getVidamax(), getVida(), 0.64), 1);
		}
	

	}

	public void dibujarMuerte() {
		if (getVida() <= 0) {
			int resto2 = muerte % 35;
			if (resto2 > 5 && resto2 <= 10) {
				setImagen(Assets.bossm0);
			} else if (resto2 > 10 && resto2 <= 15) {
				setImagen(Assets.bossm1);
			} else if (resto2 > 15 && resto2 <= 20) {
				setImagen(Assets.bossm2);
			} else if (resto2 > 20 && resto2 <= 25) {
				setImagen(Assets.bossm3);
			} else if (resto2 > 25 && resto2 <= 30) {
				setImagen(Assets.bossm4);
			} else if (resto2 > 30 && resto2 <= 35) {
				setImagen(Assets.bossm5);
			} else {
				setImagen(Assets.bossm6);
				muerto = 1;
				System.out.println("murio");
			}
		}
	}

	public void dibujarMovimiento() {
		if (transformado) {

			int resto = animacion % 40;
			if (resto > 10 && resto <= 20) {
				setImagen(Assets.bossf0);
			} else if (resto > 20 && resto <= 30) {
				setImagen(Assets.bossf1);
			} else if (resto > 30 && resto <= 40) {
				setImagen(Assets.bossf2);
			} else {
				setImagen(Assets.bossf3);
			}
		}
	}

	public void dibujarTransformacion() {
		if (!nenaviva && !(getImagen() == Assets.meta30)) {

			int resto = animacion % 90;

			if (resto > 10 && resto <= 20) {
				setImagen(Assets.meta0);
			} else if (resto > 20 && resto <= 30) {
				setImagen(Assets.meta3);
			} else if (resto > 30 && resto <= 40) {
				setImagen(Assets.meta8);
			} else if (resto > 40 && resto <= 50) {
				setImagen(Assets.meta12);
			} else if (resto > 50 && resto <= 60) {
				setImagen(Assets.meta16);
			} else if (resto > 60 && resto <= 70) {
				setImagen(Assets.meta20);
			} else if (resto > 70 && resto <= 80) {
				setImagen(Assets.meta24);
			} else if (resto > 80 && resto <= 90) {
				setImagen(Assets.meta28);
			} else {
				setImagen(Assets.meta30);
				transformado = true;

			}
		}
	}

	@Override
	public Rectangle getBoundEnemy() {
		if (getVida() > 0) {
			return new Rectangle((int) posicion.getX() + 10, (int) posicion.getY(), 128, 128);
		} else {
			// muerto = 1;
			return new Rectangle(10000, 10000, 32, 32);
		}
	}

	public Rectangle getRangoAtaque() {
		return new Rectangle((int) posicion.getX() - 100, (int) posicion.getY() - 200, 500, 300);
	}
	


	public Boolean getNenaviva() {
		return nenaviva;
	}

	public void setNenaviva(Boolean nenaviva) {
		this.nenaviva = nenaviva;
	}

	public Boolean getComienzaataque() {
		return comienzaataque;
	}

	public void setComienzaataque(Boolean comienzaataque) {
		this.comienzaataque = comienzaataque;
	}

}