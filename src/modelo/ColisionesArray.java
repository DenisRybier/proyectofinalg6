package modelo;

import java.awt.Rectangle;
import java.io.FileReader;
import java.util.ArrayList;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class ColisionesArray {
	
	private Rectangle rectangulo;
	private ArrayList<Rectangle> rectanguls;
	private JsonObject global;
	private JsonArray capas;
	
	public ColisionesArray() {
		 
	}

	public ArrayList<Rectangle> ObtenerColisiones(String ruta ){
		 ArrayList <Rectangle> rectangulos =new ArrayList();
		
		global =obtenerObjetosJson(ruta);
		
		 capas= obtenerArrayJson(global.get("layers").toString());
		
		
	for(JsonElement obj: capas) {
					
			JsonObject datos=obj.getAsJsonObject();
			
			String tipo= datos.get("type").toString();
			
		       if (tipo.equals ("\"objectgroup\""))  {
		    		
				JsonArray rectanguls = obtenerArrayJson(datos.get("objects").toString());
				
			
				for(JsonElement obje: rectanguls) {
				
					JsonObject datosR= obje.getAsJsonObject();	
					int x = obtenerDoubDesdeJson(datosR, "x");
					
					int y = obtenerDoubDesdeJson(datosR, "y");
					
					int ancho = obtenerDoubDesdeJson(datosR, "width");
					
					int alto = obtenerDoubDesdeJson(datosR, "height");	
					
					
					Rectangle rectangulo=new Rectangle(x,y,ancho,alto);
					rectangulos.add(rectangulo);	
					System.out.println(rectangulos.size());
				}
		
		
		return rectangulos;
		
		
	}}
	return null;}
	
	private JsonObject obtenerObjetosJson(final String codigo)  {
		
		JsonParser parser= new JsonParser();
		JsonObject object=null;
		try {Object objeto=parser.parse(new FileReader(codigo));
		object= (JsonObject) objeto ;}catch(Exception e) {System.out.println("Posicion");
		System.out.println(e);}
		return object;
		

		
	}
		
		private JsonArray obtenerArrayJson(final String codigo)  {
			JsonParser lector= new JsonParser();
			JsonArray array = null;
			try {Object recuperado=lector.parse(codigo);
			array=(JsonArray)recuperado;}catch(Exception e) {System.out.println("Posicion" );
			System.out.println(e);}
			return array;
			

			
		}

		
		private Integer obtenerDoubDesdeJson(final JsonObject objetoJSON, final String key) {
			return Integer.parseInt(objetoJSON.get(key).toString());
		}









public Rectangle getRectangulo() {
	return rectangulo;
}
public void setRectangulo(Rectangle rectangulo) {
	this.rectangulo = rectangulo;
}
public ArrayList<Rectangle> getRectanguls() {
	return rectanguls;
}
public void setRectanguls(ArrayList<Rectangle> rectanguls) {
	this.rectanguls = rectanguls;
}	



}


