package modelo;

import java.util.List;
import java.util.Optional;

public interface Dao <T>{

	public Optional<T> get(Integer id );
	
	
	public void save (T t);
	public void update (T t);
	public void delete (T t);
	public List<T> getAll();
	
}
