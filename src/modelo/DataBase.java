package modelo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DataBase {

	private Connection conn;
	private static DataBase db=null;
	private final String USER ="postgres";
	private final String PSW ="gaston123";
	private final String DBNAME ="DatabaseJuego";
	private final String PORT ="5432";
	private final String HOST ="localhost";
	
	
	private DataBase() {
		
		try {
			this.setConn(DriverManager.getConnection("jdbc:postgresql://"+ HOST + ":" + PORT+"/"+DBNAME,USER,PSW ));
		}catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
 
    public static DataBase getInstance() {
    	return   db!=null?  db: new DataBase();
    }
	public ResultSet query(String query) {
		ResultSet rs=null;
		try {
			
			rs=getConn().createStatement().executeQuery(query);
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return rs;
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	private Connection getConn() {
		
		return conn;
	}

	private void setConn(Connection connection) {
		this.conn=connection;
	}
	
	
	
	
}
