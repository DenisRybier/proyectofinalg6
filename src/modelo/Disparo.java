package modelo;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

import controlador.ControladorJuego;
import vista.VistaJuego;

public class Disparo extends Ente{

	private boolean vivo;
	private BufferedImage imagen;
	private Posicion posicion;
	
	final int VEL_BALA = 50;
	
	public Disparo(Posicion posicion, BufferedImage imagen, Integer vidamax, Integer vida, 
			Integer da�o,String ruta) {
		super(posicion, imagen, vidamax, vida, da�o,ruta);
		this.vivo = true;
		this.imagen = imagen;
		this.posicion = posicion;
		
	}

	@Override
	public void update() {
		
		//Analizo si la bala se fue de la vista
		
		 if (this.getPosicion().getY() + getImagen().getHeight() < 0) {
			 this.setVivo(false);
		 }
				 
		 if (this.getPosicion().getY() > VistaJuego.HEIGHT) {
			 this.setVivo(false);
		 }
		 if (this.getPosicion().getX() + getImagen().getWidth() < 0) {
			 this.setVivo(false);
		 }
		 if(this.getPosicion().getX() > VistaJuego.WIDTH) {
			 this.setVivo(false);
		 }	  
		 //cambio uno
		if((colisionesObjetos() == 1)||(colisionesObjetos() == 2)||(colisionesObjetos() == 3)
			||(colisionesObjetos() == 4)){
			this.setVivo(false);
	}					
	}
	public void seguirPersonaje(double x, double y) {

		this.getPosicion().setX(x);
		this.getPosicion().setY(y);
	}
	
	public void obtenerDireccion(Integer dir) {
		
		/* 
		 * 1 = UP
		 * 2 = LEFT
		 * 3 = DOWN
		 * 4 = RIGHT
		 */
		
		switch (dir) {
		case 1: {
			this.getPosicion().setY(this.getPosicion().getY()-VEL_BALA);
			break;
		}case 2: {
			this.getPosicion().setX(this.getPosicion().getX()-VEL_BALA);
			break;
		}case 3: {
			this.getPosicion().setY(this.getPosicion().getY()+VEL_BALA);
			break;
		}case 4: {
			this.getPosicion().setX(this.getPosicion().getX()+VEL_BALA);
			break;
		}
		default:
			throw new IllegalArgumentException("Unexpected value: " + dir);
		}
		
	}
	
	
	@Override
	public void draw(Graphics g) {
		g.setColor(Color.RED);
		if(ControladorJuego.mapa!=4 && ControladorJuego.mapa!=5) {
		g.fillRect((int)posicion.getX()+3, (int)posicion.getY()+3,4,4);
		g.drawImage(imagen, (int)posicion.getX()+3, (int)posicion.getY()+3, 4, 4, null);
		}else {
			
			g.fillRect((int)posicion.getX()+25, (int)posicion.getY()+25,6,4);
			g.drawImage(imagen, (int)posicion.getX()+25, (int)posicion.getY()+25, 6, 6, null);

		}
		   
		
	}
	public Rectangle BoundBala() {
		if(ControladorJuego.mapa!=4 && ControladorJuego.mapa!=5) {
		return new Rectangle((int)posicion.getX()+3, (int)posicion.getY()+3,4,4);}
		else {
			return new Rectangle((int)posicion.getX()+25, (int)posicion.getY()+25,6,6);}
	}


	public boolean isVivo() {
		return vivo;
	}

	public void setVivo(boolean vivo) {
		this.vivo = vivo;
	}

	public BufferedImage getImagen() {
		return imagen;
	}

	public void setImagen(BufferedImage imagen) {
		this.imagen = imagen;
	}

	public Posicion getPosicion() {
		return posicion;
	}

	public void setPosicion(Posicion posicion) {
		this.posicion = posicion;
	}

	@Override
	public Rectangle getBoundEnemy() {
		
		return null;
	}}
	