package modelo;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

import vista.VistaJuego;

public class DisparosBoss extends Ente {

	private boolean vivo;
	private BufferedImage imagen;
	private Posicion posicion;
	
	final int VEL = 45;
	
	public DisparosBoss(Posicion posicion, BufferedImage imagen) {
		super(null, null, null, null, null,null);
		this.vivo = true;
		this.imagen = imagen;
		this.posicion = posicion;
		
	}

	@Override
	public void update() {
		
		this.getPosicion().setY(this.getPosicion().getY()-VEL);	
		
		 if (this.getPosicion().getY() + getImagen().getHeight() < 0) {
			 this.setVivo(false);
		 }
				 
		 if (this.getPosicion().getY() > VistaJuego.HEIGHT) {
			 this.setVivo(false);
		 }
		 if (this.getPosicion().getX() + getImagen().getWidth() < 0) {
			 this.setVivo(false);
		 }
		 if(this.getPosicion().getX() > VistaJuego.WIDTH) {
			 this.setVivo(false);
		 }	  
		
	}					
	
	@Override
	public void draw(Graphics g) {
		   g.drawImage(imagen, (int)posicion.getX(), (int)posicion.getY(), 32, 32, null);
		
		
	}
	public Rectangle boundDisparo() {
		return new Rectangle((int)posicion.getX(), (int)posicion.getY(),40,40);}

	public boolean isVivo() {
		return vivo;
	}

	public void setVivo(boolean vivo) {
		this.vivo = vivo;
	}

	public BufferedImage getImagen() {
		return imagen;
	}

	public void setImagen(BufferedImage imagen) {
		this.imagen = imagen;
	}

	public Posicion getPosicion() {
		return posicion;
	}

	public void setPosicion(Posicion posicion) {
		this.posicion = posicion;
	}

	@Override
	public Rectangle getBoundEnemy() {
		
		return null;
	}}
