package modelo;

import java.awt.*;
import java.awt.image.*;
import java.util.ArrayList;

public abstract class Ente {
	
	protected int animacion = 0;
	protected int muerte = 0;
	public int muerto = 0;


	protected BufferedImage imagen;
	protected Posicion posicion;
	protected Integer vidamax, vida, da�o;
	protected int longitud;
	protected double percentage, doublong;
    protected String ruta;
	
	protected Rectangle izquierda, derecha, arriba, abajo;

	
	public Ente(Posicion posicion, BufferedImage imagen, Integer vidamax, Integer vida, 
			Integer da�o,String ruta) {
		this.posicion = posicion;
		this.imagen = imagen;
		this.vidamax = vidamax;
		this.vida = vida;
		this.da�o = da�o;
		this.ruta=ruta;
	}
	public abstract void update();
	
	public abstract void draw(Graphics g);
	
	
	public void animarEnte() {
		if (animacion < 32767) {
			animacion++;
		} else {
			animacion = 0;
		}
	}
	
	public void animarMuerte() {
		if (muerte < 32767) {
			muerte++;
		} else {
			muerte = 0;
		}
	}
	
	
	public int colisionesObjetos() {
		//System.out.println("n");
		Rectangle rectangulo = new Rectangle();
		ArrayList<Rectangle> rectangulos = new ArrayList<Rectangle>();
		ColisionesArray colisionesObjetos = new ColisionesArray();
		rectangulos = colisionesObjetos.ObtenerColisiones(this.ruta);
		for (int i = 0; i < rectangulos.size(); i++) {
			rectangulo = rectangulos.get(i);
			//System.out.println(rectangulo.x);
			//System.out.println(rectangulo.width);
			//System.out.println(posicion.getX());
			if (getBoundsIzquierda().intersects(rectangulo)) {
				System.out.println("colisiono");
				return 1;
			}
			if (getBoundsDerecha().intersects(rectangulo)) {
				System.out.println("colisiono");
				return 2;
			}
			if (getBoundsArriba().intersects(rectangulo)) {
				System.out.println("colisiono");
				return 3;
			}
			if (getBoundsAbajo().intersects(rectangulo)) {
				System.out.println("colisiono");
				return 4;
			}

		}
		return 0;
	}
	
	
	
	
	
	
	
	
	
	
public Rectangle getRangoAtaque() {
		
		return new Rectangle((int) posicion.getX()+8 , (int) posicion.getY()+8, 200, 200);
	}

	
	public Rectangle getBoundsArriba() {

		return new Rectangle((int) posicion.getX() + 2, (int) posicion.getY()-1, 14, 2);
	}

	public Rectangle getBoundsAbajo() {

		return new Rectangle((int) posicion.getX() + 2, (int) posicion.getY() + 15, 14, 4);
	}

	public Rectangle getBoundsDerecha() {

		return new Rectangle((int) posicion.getX() + 16, (int) posicion.getY() + 3, 2, 12);
	}

	public Rectangle getBoundsIzquierda() {

		return new Rectangle((int) posicion.getX()-1, (int) posicion.getY() + 3, 2, 12);
	}
	

	public int colisionesAtaque() {
		if(getVida() <= 0) {
			return 1;
		}
		else {
		return 0;}
	}
	


	public int getLongitudBarra(int max, int actual, double tama�o) {

		int percentage = actual*100/max;
		double doublong = (percentage*tama�o);
		setLongitud((int) doublong);
		return getLongitud();
	}
	
	
	
	

	public int getLongitud() {
		return longitud;
	}

	public void setLongitud(int longitud) {
		this.longitud = longitud;
	}

	public Integer getVidamax() {
		return vidamax;
	}

	public void setVidamax(Integer vidamax) {
		this.vidamax = vidamax;
	}
	
	
	public abstract Rectangle getBoundEnemy(); 

	public Integer getVida() {
		return vida;
	}

	public void setVida(Integer vida) {
		this.vida = vida;
	}


	public int getMuerto() {
		return muerto;
	}
	public void setMuerto(int muerto) {
		this.muerto = muerto;
	}
	public Integer getDa�o() {
		return da�o;
	}

	public void setDa�o(Integer da�o) {
		this.da�o = da�o;
	}

	public Posicion getPosicion() {
		return posicion;
	}

	public BufferedImage getImagen() {
		return imagen;
	}

	public void setImagen(BufferedImage imagen) {
		this.imagen = imagen;
	}

	public void setPosicion(Posicion posicion) {
		this.posicion = posicion;
	}

	public boolean collision(Rectangle area) {
		return false;
	}
	public String getRuta() {
		return ruta;
	}
	public void setRuta(String ruta) {
		this.ruta = ruta;
	}
	

	
}
