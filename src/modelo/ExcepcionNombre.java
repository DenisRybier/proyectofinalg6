package modelo;

public class ExcepcionNombre extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public String getMessage() {
		return "Nombre incorrecto, vuelva a ingresar el nombre";
	}
	
	

}
