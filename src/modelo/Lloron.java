package modelo;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

public class Lloron extends Ente {
	
	int VEL = 7;
	int movY = 0;
	int movX=0;
	int r = (int) (Math.random() * 100) + 1;

	public Lloron(Posicion posicion, BufferedImage imagen, Integer vidamax, Integer vida, Integer da�o, String ruta) {
		super(posicion, imagen, vidamax, vida, da�o, ruta);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void update() {
		
		if (Personaje.enpausa == false) {

		
		animarEnte();

		movX++;
		if (movX < r) {
			posicion.setX(posicion.getX() - VEL);
			
		} else {
			movY++;
			if (movY < 30) {
				posicion.setY(posicion.getY() + VEL);
				
				
			} else {
				movX = 0;
				movY = 0;
				VEL = -VEL;
			}
		}

		int resto = animacion % 8;
	

			if (resto > 2 && resto <= 4) {
				setImagen(Assets.lloron);
			} else if (resto > 4 && resto <= 6) {
				setImagen(Assets.lloron1);
			} else if (resto > 6) {
				setImagen(Assets.lloron2);
			} else {
				setImagen(Assets.lloron3);
			}

			
		
		
		}
		
		
	}

	@Override
	public void draw(Graphics g) {
		if (getVida()>0) {
			
			g.drawImage(imagen, (int) posicion.getX(), (int) posicion.getY(), null);
			g.setColor(Color.RED);

			g.drawRect((int) posicion.getX() + 6, (int) posicion.getY(), 
					getLongitudBarra(getVidamax(), getVida(), 0.16), 1);

			}
		
	}

	@Override
	public Rectangle getBoundEnemy() {
		if (getVida()>0) {
		return new Rectangle ((int) posicion.getX() , (int) posicion.getY() , 32, 32);
		}
		else {
	    muerto=1;
		return new Rectangle (10000 ,10000 , 2, 2);}
	}
  public Rectangle getRangoAtaque() {
	  return new Rectangle ((int) posicion.getX()+8 , (int) posicion.getY()+8 , 50, 50);
  }
	
	
	
	
	
}
