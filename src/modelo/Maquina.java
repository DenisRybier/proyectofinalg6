package modelo;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

public class Maquina extends Ente {
	

	public Maquina(Posicion posicion, BufferedImage imagen, Integer vidamax, Integer vida, Integer da�o, String ruta) {
		super(posicion, imagen, vidamax, vida, da�o, ruta);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void update() {
		
		if (Personaje.enpausa == false) {

		animarEnte();

	

		int resto = animacion % 10;
	 

			if (resto > 2 && resto <= 4) {
				setImagen(Assets.maquina1);
			} else if (resto > 4 && resto <= 6) {
				setImagen(Assets.maquina2);
			} else if (resto > 6&& resto <= 8) {
				setImagen(Assets.maquina3);
			} else if (resto>8){
				setImagen(Assets.maquina4);
			} else {
				setImagen(Assets.maquina5);
			}

			
		
		
		}
		
		
	}

	@Override
	public void draw(Graphics g) {
		if (getVida()>0) {
			
			g.drawImage(imagen, (int) posicion.getX(), (int) posicion.getY(), null);
			g.setColor(Color.RED);

			g.drawRect((int) posicion.getX() + 6, (int) posicion.getY(), 
					getLongitudBarra(getVidamax(), getVida(), 0.16), 1);

			}
		
		if(getVida()<=0) {
			
			g.drawImage(Assets.maquinaf, (int) posicion.getX(), (int) posicion.getY(), null);
			g.setColor(Color.RED);
		}
		
	}

	@Override
	public Rectangle getBoundEnemy() {
		if (getVida()>0) {
		return new Rectangle ((int) posicion.getX() , (int) posicion.getY() , 128, 128);
		}
		else {
	    muerto=1;
		return new Rectangle (10000 ,10000 , 2, 2);}
	}
  public Rectangle getRangoAtaque() {
	  return new Rectangle ((int) posicion.getX()+8 , (int) posicion.getY()+8 , 0, 0);
  }
	
	
}
