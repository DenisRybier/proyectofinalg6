package modelo;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

public class NenaRoja extends Ente {

	int VEL = 7;
	int movY = 0;
	int movX=0;
	int r = (int) (Math.random() * 100) + 1;

	public NenaRoja(Posicion posicion, BufferedImage imagen, Integer vidamax, Integer vida, Integer da�o, String ruta) {
		super(posicion, imagen, vidamax, vida, da�o, ruta);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void update() {
		
		if (Personaje.enpausa == false) {

		setImagen(Assets.roja);
		animarEnte();

		

					
		}
		}
		
	 

	@Override
	public void draw(Graphics g) {
		if (getVida()>0) {
			
			g.drawImage(imagen, (int) posicion.getX(), (int) posicion.getY(), null);
			g.setColor(Color.RED);

			g.drawRect((int) posicion.getX() + 6, (int) posicion.getY(), 
					getLongitudBarra(getVidamax(), getVida(), 0.16), 1);

			}
		
	}

	@Override
	public Rectangle getBoundEnemy() {
		if (getVida()>0) {
		return new Rectangle ((int) posicion.getX() , (int) posicion.getY() ,64,64);
		}
		else {
	    muerto=1;
		return new Rectangle (10000 ,10000 , 2, 2);}
	}
  public Rectangle getRangoAtaque() {
	  return new Rectangle ((int) posicion.getX()+8 , (int) posicion.getY()+8 ,300, 300);
  }
	
	
	
	
}
