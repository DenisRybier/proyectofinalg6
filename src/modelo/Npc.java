package modelo;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

public class Npc extends Ente{
 private Boolean transformada =false;
 private Boolean fasefinal=false;
	public Npc(Posicion posicion, BufferedImage imagen, Integer vidamax, Integer vida, Integer da�o, String ruta) {
		super(posicion, imagen, vidamax, vida, da�o, ruta);
		// TODO Auto-generated constructor stub
	}

	@Override
public void update() {
		
		if(!(getVida()<0)) {
		setImagen(imagen);
		
		
		if (transformada) {
			animarEnte();
			animarMuerte();
			
			int resto = animacion % 60;

			if (resto > 10 && resto <= 20) {
				setImagen(Assets.abuet);
			} else if (resto > 20 && resto <= 30) {
				setImagen(Assets.abuet2);
			} else if (resto > 30 && resto <= 40) {
				setImagen(Assets.abuet3);
			} else if (resto > 40 && resto <= 50) {
				setImagen(Assets.abuet4);
			} else  {
				setImagen(Assets.abuet5);
			
		}
			//transformada=false;
			//fasefinal=true;
	}

		}else { setImagen(Assets.abuelaF);}
	
	}


	@Override
	public void draw(Graphics g) {

			
			g.drawImage(imagen, (int) posicion.getX(), (int) posicion.getY(), null);
		
	}

	@Override
	public Rectangle getBoundEnemy() {
		if(!(getVida()<0)) {
		return new Rectangle ((int) posicion.getX() , (int) posicion.getY() , 128, 128);
	} else return new Rectangle (10000 , 10000 , 64, 64);  }
	
	

	public Boolean getTransformada() {
		return transformada;
	}

	public void setTransformada(Boolean transformada) {
		this.transformada = transformada;
	}

}
