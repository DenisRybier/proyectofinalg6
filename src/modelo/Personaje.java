package modelo;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.swing.JOptionPane;

import controlador.ControladorJuego;
import controlador.Teclado;
import javazoom.jlgui.basicplayer.BasicPlayer;
import javazoom.jlgui.basicplayer.BasicPlayerException;
import vista.VistaJuego;

public class Personaje extends Ente {

	final int VEL = 4;
	public String nombre = "";
	private Integer municion;
	private boolean elDispara;
	private Integer direccionDisparo;
	private boolean tieneMunicion;
	int temp;
	public int nivel = 1;
	private int expmax = 20;
	public int exp = 0;
	private int vidas = 3;
	private int width, height;
	public static boolean enpausa = false;
	private Boolean x = false;
	private boolean x2 = false;
	private boolean x3 = false;
	private Integer numMapa;

	public Personaje(Posicion posicion, BufferedImage imagen, Integer vidamax, Integer vida, Integer municion,
			Integer da�o, boolean tieneMunicion, int vidas, String ruta, Integer numMapa) {
		super(posicion, imagen, vidamax, vida, da�o, ruta);
		setMunicion(municion);
		this.tieneMunicion = tieneMunicion;
		this.setTemp(temp);
		this.vidas = vidas;
		this.numMapa = numMapa;
	}

	@Override
	public void update() {

		if (Personaje.enpausa == false) {

			animarEnte();

			animarMuerte();
			int resto2 = muerte % 4;

			setearImagenMovimiento();

		}
		if (Teclado.P) {
			enpausa = !enpausa;
		}
		if (Teclado.X) {
			if (ControladorJuego.mapa == 3) {
				x = true;
				if (ControladorJuego.bossmuerto) {
					x3 = true;
				}
			}
			if (ControladorJuego.mapa == 4) {
				x2 = true;
			}

		}
	}

	@Override
	public void draw(Graphics g) {
		// DIBUJO DE PERSONAJE Y BARRA DE VIDA

		if (!(getImagen() == Assets.personajeDeath3)) {
			g.drawImage(imagen, (int) posicion.getX(), (int) posicion.getY(), null);
			g.setColor(Color.GREEN);
			if (ControladorJuego.mapa != 4) {
				if (getVida() >= 0) {
					g.drawRect((int) posicion.getX(), (int) posicion.getY(),
							getLongitudBarra(getVidamax(), getVida(), 0.16), 1);
				}
			} else {
				if (getVida() >= 0) {
					g.fillRect((int) posicion.getX() + 6, (int) posicion.getY() - 5,
							getLongitudBarra(getVidamax(), getVida(), 0.64), 3);
				}
			}
		}

		// DIBUJO DE COORDENADAS
		g.setColor(Color.WHITE);
		g.setFont(new Font("System", 1, 12));
		g.drawString("X: " + Double.toString(posicion.getX()), 10, 20);
		g.drawString("Y: " + Double.toString(posicion.getY()), 10, 35);

		// VIDA ACTUAL, MUNICIONES

		g.drawImage(Assets.health, 110, 8, null);
		g.drawString(Double.toString(getVida()), 133, 21);
		g.drawImage(Assets.gun, 110, 30, null);
		if (tieneMunicion) {
			g.drawString(Double.toString((int) (getMunicion())), 133, 43);
		}
		g.drawString("VIDAS: " + Double.toString(getVidas()), 110, 65);
		g.drawString("DA�O: " + getDa�o(), 110, 87);

		// BARRA DE EXPERIENCIA
		g.drawString("EXP ", 965, 28);
		g.drawRect(1000, 20, 175, 7);
		g.setColor(Color.MAGENTA);
		g.fillRect(1001, 21, getLongitudBarra(expmax, getExp(), 1.73), 5);

		g.drawString("NIVEL: " + getNivel(), 965, 48);

		// NOMBRE
		g.setColor(Color.WHITE);
		if (ControladorJuego.mapa != 4) {
			g.drawString(getNombre(), (int) posicion.getX() - 3, (int) posicion.getY() - 7);
		} else {
			g.setFont(new Font("System", 1, 24));
			g.drawString(getNombre(), (int) posicion.getX() + 6, (int) posicion.getY() - 10);
		}

	}

	public void setearImagenMovimiento() {
		if (!(colisionesAtaque() == 1)) {
			if (Teclado.UP) {
				this.setDireccionDisparo(1);
				if (!(colisionesObjetos() == 3) || colisionesObjetos() == 0) {
					posicion.setY(posicion.getY() - VEL);
				}

				if (posicion.getY() < 0) {
					System.out.println("limite total arriba");
					posicion.setY(posicion.getY() + VEL);

				}

				if (ControladorJuego.mapa != 4 && ControladorJuego.mapa != 5) {
					setImagen(Assets.personaje);

					if (animacion % 15 > 5 && animacion % 15 <= 10) {
						setImagen(Assets.personajeUp2);
					} else if (animacion % 15 > 10 && animacion % 15 <= 15) {
						setImagen(Assets.personajeUp3);
					} else {
						setImagen(Assets.personajeUp);
					}

				} else {
					setImagen(Assets.persrescal);
					if (animacion % 15 > 5 && animacion % 15 <= 10) {
						setImagen(Assets.persrescalup);
					} else if (animacion % 15 > 10 && animacion % 15 <= 15) {
						setImagen(Assets.persrescalup2);
					} else {
						setImagen(Assets.persrescalup3);
					}

				}

			}

			if (Teclado.LEFT) {
				this.setDireccionDisparo(2);
				if (!(colisionesObjetos() == 1) || colisionesObjetos() == 0) {
					posicion.setX(posicion.getX() - VEL);
				}

				if (posicion.getX() < 0) {
					System.out.println("limite total izquierda");
					posicion.setX(posicion.getX() + VEL);
				}

				if (ControladorJuego.mapa != 4 && ControladorJuego.mapa != 5) {

					setImagen(Assets.personaje);
					if (animacion % 15 > 5 && animacion % 15 <= 10) {
						setImagen(Assets.personajeLeft);
					} else if (animacion % 15 > 10 && animacion % 15 <= 15) {
						setImagen(Assets.personajeLeft2);
					} else {
						setImagen(Assets.personajeLeft3);
					}

				} else {
					setImagen(Assets.persrescal);

					if (animacion % 15 > 5 && animacion % 15 <= 10) {
						setImagen(Assets.persrescalleft);
					} else if (animacion % 15 > 10 && animacion % 15 <= 15) {
						setImagen(Assets.persrescalleft2);
					} else {
						setImagen(Assets.persrescalleft3);
					}

				}

			}

			if (Teclado.DOWN) {
				this.setDireccionDisparo(3);
				if (!(colisionesObjetos() == 4) || colisionesObjetos() == 0) {
					posicion.setY(posicion.getY() + VEL);
				}

				if (posicion.getY() > VistaJuego.HEIGHT - 50) {
					System.out.println("limite total abajo");
					posicion.setY(posicion.getY() - VEL);
				}

				if (ControladorJuego.mapa != 4 && ControladorJuego.mapa != 5) {
					setImagen(Assets.personaje);
					if (animacion % 15 > 5 && animacion % 15 <= 10) {
						setImagen(Assets.personaje);
					} else if (animacion % 15 > 10 && animacion % 15 <= 15) {
						setImagen(Assets.personaje2);
					} else {
						setImagen(Assets.personaje3);
					}

				} else {
					setImagen(Assets.persrescal);

					if (animacion % 15 > 5 && animacion % 15 <= 10) {
						setImagen(Assets.persrescal);
					} else if (animacion % 15 > 10 && animacion % 15 <= 15) {
						setImagen(Assets.persrescal2);
					} else {
						setImagen(Assets.persrescal3);
					}
				}

			}

			if (Teclado.RIGHT) {
				this.setDireccionDisparo(4);
				if (!(colisionesObjetos() == 2) || colisionesObjetos() == 0) {
					posicion.setX(posicion.getX() + VEL);
				}

				if (posicion.getX() > VistaJuego.WIDTH - 30) {
					System.out.println("limite total der");
					posicion.setX(posicion.getX() - VEL);
				}

				if (ControladorJuego.mapa != 4 && ControladorJuego.mapa != 5) {
					setImagen(Assets.personaje);
					if (animacion % 15 > 5 && animacion % 15 <= 10) {
						setImagen(Assets.personajeRight);
					} else if (animacion % 15 > 10 && animacion % 15 <= 15) {
						setImagen(Assets.personajeRight2);
					} else {
						setImagen(Assets.personajeRight3);
					}

				} else {
					setImagen(Assets.persrescal);
					if (animacion % 15 > 5 && animacion % 15 <= 10) {
						setImagen(Assets.persrescalright);
					} else if (animacion % 15 > 10 && animacion % 15 <= 15) {
						setImagen(Assets.persrescalright2);
					} else {
						setImagen(Assets.persrescalright3);
					}

				}

			}

			if (Teclado.E) {
				this.setElDispara(true);

				this.setMunicion(this.getMunicion() - 1);

				if (this.getMunicion() == 0) {
					this.setTieneMunicion(false);
				}

				if (this.isTieneMunicion()) {
					try {
						BasicPlayer basic = new BasicPlayer();
						basic.open(new File(Assets.disparo));
						basic.play();
					} catch (BasicPlayerException e) {
						e.printStackTrace();
					}
				}

			}
		} else {
			reAparecer(this.getNumMapa());
		}
	}

	public void aumentarExp() {
		setExp(getExp() + 20);
		actualizarNivel();
	}

	public void actualizarNivel() {
		if (getExp() >= 30) {
			setNivel(getNivel() + 1);
			setExpmax((int) (getExpmax() * 1.25));
			setMunicion(getMunicion() + 10);
			setExp(0);
			setDa�o(getDa�o() + 5);
		}
	}

	public Rectangle boundVista() {
		return new Rectangle((int) posicion.getX(), (int) posicion.getY(), 400, 400);
	}

	public void reAparecer(Integer numMapa) {
		if (!(getVidas() <= 1)) {
			if (ControladorJuego.mapa < 4) {
				setImagen(Assets.personaje);
			} else {
				setImagen(Assets.persrescal);
			}

			if (numMapa == 1) {

				posicion.setX(832);
				posicion.setY(344);

			} else if (numMapa == 2) {

				posicion.setX(298);
				posicion.setY(632);

			} else if (numMapa == 3) {

				posicion.setX(768.5);
				posicion.setY(590);

			}

			setVida(250);
			setVidas(getVidas() - 1);
		} else {
			setVidas(3);
			VistaJuego.juegoTerminado = true;
			JOptionPane.showMessageDialog(null, "GAME OVER");
		}
	}

	public double obtenerX() {
		return this.getPosicion().getX();
	}

	public double obtenerY() {
		return this.getPosicion().getY();
	}

	public int getMunicion() {
		return municion;
	}

	public void setMunicion(int municion) {
		this.municion = municion;
	}

	public int getNivel() {
		return nivel;
	}

	public void setNivel(int nivel) {
		this.nivel = nivel;
	}

	public int getExp() {
		return exp;
	}

	public void setExp(int exp) {
		this.exp = exp;
	}

	public boolean isElDispara() {
		return elDispara;
	}

	public void setElDispara(boolean elDispara) {
		this.elDispara = elDispara;
	}

	public Integer getDireccionDisparo() {
		return direccionDisparo;
	}

	public void setDireccionDisparo(Integer direccionDisparo) {
		this.direccionDisparo = direccionDisparo;
	}

	public boolean isTieneMunicion() {
		return tieneMunicion;
	}

	public void setTieneMunicion(boolean tieneMunicion) {
		this.tieneMunicion = tieneMunicion;
	}

	public int getTemp() {
		return temp;
	}

	public void setTemp(int temp) {
		this.temp = temp;
	}

	@Override
	public Rectangle getBoundEnemy() {
		// TODO Auto-generated method stub
		return null;
	}

	public int getExpmax() {
		return expmax;
	}

	public void setExpmax(int expmax) {
		this.expmax = expmax;
	}

	public int getVidas() {
		return vidas;
	}

	public void setVidas(int vidas) {
		this.vidas = vidas;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public Boolean getX() {
		return x;
	}

	public void setX(Boolean x) {
		this.x = x;
	}

	public Integer getNumMapa() {
		return numMapa;
	}

	public void setNumMapa(Integer numMapa) {
		this.numMapa = numMapa;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public boolean isX2() {
		return x2;
	}

	public void setX2(boolean x2) {
		this.x2 = x2;
	}

	public boolean isX3() {
		return x3;
	}

	public void setX3(boolean x3) {
		this.x3 = x3;
	}

}