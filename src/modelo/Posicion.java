package modelo;

public class Posicion {
	
	private double x,y;
	
	public Posicion(double x, double y) {
		this.setX(x);
		this.setY(y);
	}
	
	public Posicion() {
		setX(0);
		setY(0);
	}
	
	public Posicion agregar(Posicion p) {
		return new Posicion(x + p.getX(), y + p.getY());
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}
	
	

}
