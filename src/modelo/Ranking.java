package modelo;

public class Ranking {
   
	Integer id;
	String usuario;
	String tiempo;
	String vida;
	String Valoracion;
	String puntos;
	public Ranking(Integer id, String usuario, String tiempo, String vida, String valoracion,String puntos) {
		super();
		this.id=id;
		this.usuario = usuario;
		this.tiempo = tiempo;
		this.vida = vida;
		this.Valoracion = valoracion;
		this.puntos=puntos;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getTiempo() {
		return tiempo;
	}
	public void setTiempo(String tiempo) {
		this.tiempo = tiempo;
	}
	public String getVida() {
		return vida;
	}
	public void setVida(String vida) {
		this.vida = vida;
	}
	public String getValoracion() {
		return Valoracion;
	}
	public void setValoracion(String valoracion) {
		Valoracion = valoracion;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getPuntos() {
		return puntos;
	}
	public void setPuntos(String puntos) {
		this.puntos = puntos;
	}
	
	
	
}
