package modelo;
import java.io.File;
import javazoom.jlgui.basicplayer.BasicPlayer;
import javazoom.jlgui.basicplayer.BasicPlayerException;

public class Reproductor {

	private BasicPlayer repro;
	
	public Reproductor() {
		super();
		repro = new BasicPlayer();
	}
	
	public void especificarSonido(String path)  {
		

			try {
				abrir(path);
				play();
			} catch (BasicPlayerException e) {
				e.printStackTrace();
			}
		}
	
	public void detenerSonido() {
		
		try {
			stop();
		} catch (BasicPlayerException e) {
			e.printStackTrace();
		}	
			 
	}
	
	public void reanudarSonido() {
		
		try {
			resume();
		} catch (BasicPlayerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
			 
	}
	
	public void pausarSonido() {
		
		try {
			pausa();
		} catch (BasicPlayerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
			 
	}
	
	public void abrir(String ruta) throws BasicPlayerException {
		repro.open(new File(ruta));
	}
	
	public void play() throws BasicPlayerException {
		repro.play(); 
	}
	
	public void stop() throws BasicPlayerException {
		repro.stop();
	}
	
	public void pausa() throws BasicPlayerException {
		repro.pause();
	}
	
	public void resume() throws BasicPlayerException {
		repro.resume();
	}

	public BasicPlayer getRepro() {
		return repro;
	}

	public void setRepro(BasicPlayer repro) {
		this.repro = repro;
	}
	
	
}
