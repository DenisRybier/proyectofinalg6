package modelo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


  public class ResumenDao  implements Dao<Ranking>{
  private DataBase db= DataBase.getInstance();
	
	@Override
	public Optional <Ranking>get(Integer id) {
		Ranking ranking = null;
	    ResultSet rs= getDb().query("SELECT * FROM ranking1 WHERE ranking1.id= " + id + "LIMIT 1" );
		try {
				while (rs.next()) {
			String usuario =rs.getString(2);
			String tiempo =rs.getString(3);
			String vida =rs.getString(4);
		    String valoracion =rs.getString(5);
		    String puntos=rs.getString(6);
		    ranking = new Ranking(id,usuario,tiempo,vida,valoracion,puntos);
		}
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return Optional.ofNullable(ranking);
	}

	

	@Override
	public void save(Ranking t) {
		ResultSet rs= getDb().query("INSERT INTO public.ranking1 (usuario,tiempo,vidas,valoracion,puntos) VALUES ( "
				+"'"+ t.getUsuario()+"'::character varying,'" +
				t.getTiempo()+"'::character varying, '"+
				t.getVida()+"'::character varying, '"+
				t.getValoracion()+"'::character varying,'" +
				t.getPuntos()+"'::character varying ) " +
				" returning id;");
		
	}

	@Override
	public void update(Ranking t) {
		ResultSet rs= getDb().query("UPDATE public.ranking1 set usuario= ' "
				+ t.getUsuario()+"'::character varying," +
				"tiempo = '" +t.getTiempo()+"'::character varying, "+
				"vida  = ' " +t.getVida()+"'::character varying, "+
				"valoracion  =  '"+t.getValoracion()+"'::character varying WHERE id" +
				t.getId()+";");
		
	}

	@Override
	public void delete(Ranking t) {
		ResultSet rs= getDb().query("DELETE public.ranking1 WHERE id IN (" + t.getId()+");" );
		
	}
	@Override
	public List<Ranking> getAll() {
		ResultSet rs= getDb().query("SELECT * FROM ranking1 order by CAST (puntos as int) desc; ");
		List <Ranking> rankings = new ArrayList<Ranking>();
		
		try {
				while (rs.next()) {
			Integer id=rs.getInt(1);
			String usuario =rs.getString(2);
			String tiempo =rs.getString(3);
			String vida =rs.getString(4);
		    String valoracion =rs.getString(5);
		    String puntos=rs.getString(6);
		   Ranking ranking = new Ranking(id,usuario,tiempo,vida,valoracion,puntos);
		   rankings.add(ranking);
		}
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return rankings;
	}
	

public Integer maxId() {
	Integer id=1;
	System.out.println("entre a maxid");
	ResultSet rs= DataBase.getInstance().query("SELECT id FROM public.ranking1 ORDER BY id DESC LIMIT 1;");
	System.out.println("lo pase al maxid");
	try {
		if(rs.next()) {
			id=rs.getInt(1);
		}
	} catch (SQLException e) {
		e.printStackTrace();
	}
	return id;
}

	public DataBase getDb() {
		return db;
	}



	public void setDb(DataBase db) {
		this.db = db;
	}



	

	
	
}
