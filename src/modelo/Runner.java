package modelo;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.Random;

public class Runner extends Ente {

	int VEL = 3;
	int movY = 0;
	int r = (int) (Math.random() * 100) + 1;
    Boolean ataca=false;
	public Runner(Posicion posicion, BufferedImage imagen, Integer vidamax, Integer vida, Integer da�o, String ruta) {
		super(posicion, imagen, vidamax, vida, da�o, ruta);
	}

	@Override
	public void update() {

		if (Personaje.enpausa == false) {
if(!ataca) {
			animarEnte();
            
			/*
			 * if (accion < 32767) { accion++; }
			 */

			int resto = animacion % 8;
			// int resto2 = accion % 60;

			// if (!(getAccion() == 1)) {

			if (resto > 2 && resto <= 4) {
				setImagen(Assets.runner);
			} else if (resto > 4 && resto <= 6) {
				setImagen(Assets.runner2);
			} else if (resto > 6) {
				setImagen(Assets.runner3);
			} else {
				setImagen(Assets.runner4);
			}

			movY++;
			if (movY < r) {

				posicion.setY(posicion.getY() + VEL);

			} else {
				movY = 0;
				VEL = -VEL;
			}
		}
		else {
			animarMuerte();
			int resto2 = muerte % 8;
			if (resto2 > 2 && resto2 <= 4) {
				setImagen(Assets.runnerA1);
			} else if (resto2 > 4 && resto2 <= 6) {
				setImagen(Assets.runnerA2);
			} else if (resto2 > 6) {
				setImagen(Assets.runnerA3);
			} else {
				setImagen(Assets.runnerA4);
			}
			ataca=false;
		}
		}
	}

	@Override
	public void draw(Graphics g) {
		if (getVida() > 0) {
			g.drawImage(imagen, (int) posicion.getX(), (int) posicion.getY(), null);
			g.setColor(Color.RED);

			g.drawRect((int) posicion.getX() + 6, (int) posicion.getY(),
					getLongitudBarra(getVidamax(), getVida(), 0.16), 1);

		}
	}

	public int ColisionesAtaque() {
		if (getVida() <= 0) {
			muerto = 1;
		}
		return muerto;
	}

	public Rectangle getRangoAtaque() {

		return new Rectangle((int) posicion.getX() - 32, (int) posicion.getY() - 32, 80, 80);
	}

	@Override
	public Rectangle getBoundEnemy() {
		if (getVida() > 0) {
			return new Rectangle((int) posicion.getX(), (int) posicion.getY(), 20, 20);
		} else {
			return new Rectangle(10000, 10000, 20, 20);
		}
	}

	public Boolean getAtaca() {
		return ataca;
	}

	public void setAtaca(Boolean ataca) {
		this.ataca = ataca;
	}

}
