package modelo;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

import vista.VistaJuego;

public class Spider extends Ente {

	int VEL = 6;
	private int movX, movY = 0;
	public int temp;
	int r = (int) (Math.random() * 130) + 1;

	public Spider(Posicion posicion, BufferedImage imagen, Integer vidamax, Integer vida, Integer da�o, String ruta) {
		super(posicion, imagen, vidamax, vida, da�o, ruta);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void update() {

		if (Personaje.enpausa == false) {

			animarEnte();

			animarMuerte();

			if (!(colisionesAtaque() == 1)) {

				dibujarMovimiento();

				movX++;
				if (movX < r) {
					posicion.setX(posicion.getX() - VEL);

				} else {
					movY++;
					if (movY < 30) {
						posicion.setY(posicion.getY() + VEL);

					} else {
						movX = 0;
						movY = 0;
						VEL = -VEL;
					}
				}
			} else {

				if (!(this.temp == 2)) {
					dibujarMuerte();
				}
			}

			// }
		}
	}

	@Override
	public void draw(Graphics g) {
		g.drawImage(imagen, (int) posicion.getX(), (int) posicion.getY(), null);
		g.setColor(Color.RED);
		g.drawRect((int) posicion.getX() - 2, (int) posicion.getY() - 10,
				getLongitudBarra(getVidamax(), getVida(), 0.64), 1);

	}

	public Rectangle getRangoAtaque() {

		return new Rectangle((int) posicion.getX() + 8, (int) posicion.getY() + 8, 420, 420);
	}

	public void dibujarMovimiento() {
		int resto = animacion % 40;

		if (resto > 10 && resto <= 20) {
			setImagen(Assets.spider1);
		} else if (resto > 20 && resto <= 30) {
			setImagen(Assets.spider2);
		} else if (resto > 30) {
			setImagen(Assets.spider3);
		} else {
			setImagen(Assets.spider4);
		}
	}

	public void dibujarMuerte() {
		int resto2 = muerte % 70;
		if (resto2 > 10 && resto2 <= 20) {
			setImagen(Assets.spiderDeath);
			DeathSleep();
		} else if (resto2 > 20 && resto2 <= 30) {
			setImagen(Assets.spiderDeath2);
			DeathSleep();
		} else if (resto2 > 30 && resto2 <= 40) {
			setImagen(Assets.spiderDeath3);
			DeathSleep();
		} else if (resto2 > 40 && resto2 <= 50) {
			setImagen(Assets.spiderDeath4);
			DeathSleep();
		} else if (resto2 > 50 && resto2 <= 60) {
			setImagen(Assets.spiderDeath5);
			DeathSleep();
		} else {
			setImagen(Assets.spiderDeath6);
			DeathSleep();
			this.temp = 2;
			muerto = 1;
		}
	}

	public void DeathSleep() {
		try {
			Thread.sleep(10);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Rectangle getBoundEnemy() {

		if (getVida() > 0) {
			return new Rectangle((int) posicion.getX() + 10, (int) posicion.getY(), 64, 64);
		} else {
			// muerto = 1;
			return new Rectangle(10000, 10000, 32, 32);
		}
	}

}
