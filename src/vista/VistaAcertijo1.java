package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controlador.ControladorAcertijo;
import modelo.Assets;

import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;

public class VistaAcertijo1 extends JFrame {

	private JPanel contentPane;
	private JButton SibCentral,LabCentral,SolbCentral,MibCentral,RebCentral,SiCentral,LaCentral,SolCentral,FaCentral,MiCentral,
	ReCentral,DoCentral,Reb,Mib,Solb,Lab,Sib,Si,La,Sol,Fa,Mi,Re,Do;
    private ControladorAcertijo controlador;
	/**
	 * Launch the application.
	 */
	

	/**
	 * Create the frame.
	 */
	public VistaAcertijo1(ControladorAcertijo controlador) {
		this.setControlador(controlador);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 524, 319);
	    contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		SibCentral = new JButton("");
		SibCentral.addActionListener(getControlador());
		SibCentral.setIcon(new ImageIcon(VistaAcertijo1.class.getResource("/recursos_mapa/teclaNegra.png")));
		SibCentral.setBounds(459, 11, 21, 173);
		contentPane.add(SibCentral);
		
		LabCentral = new JButton("");
		LabCentral.addActionListener(getControlador());
		LabCentral.setIcon(new ImageIcon(VistaAcertijo1.class.getResource("/recursos_mapa/teclaNegra.png")));
		LabCentral.setBounds(419, 11, 21, 173);
		contentPane.add(LabCentral);
		
		SolbCentral = new JButton("");
		SolbCentral.addActionListener(getControlador());
		SolbCentral.setIcon(new ImageIcon(VistaAcertijo1.class.getResource("/recursos_mapa/teclaNegra.png")));
		SolbCentral.setBounds(376, 11, 21, 173);
		contentPane.add(SolbCentral);
		
		MibCentral = new JButton("");
		MibCentral.setIcon(new ImageIcon(VistaAcertijo1.class.getResource("/recursos_mapa/teclaNegra.png")));
		MibCentral.addActionListener(getControlador());
		MibCentral.setBounds(316, 11, 21, 173);
		contentPane.add(MibCentral);
		
		RebCentral = new JButton("");
		RebCentral.addActionListener(getControlador());
		RebCentral.setIcon(new ImageIcon(VistaAcertijo1.class.getResource("/recursos_mapa/teclaNegra.png")));
		RebCentral.setBounds(272, 11, 21, 173);
		contentPane.add(RebCentral);
		
		SiCentral = new JButton("");
		SiCentral.addActionListener(getControlador());
		SiCentral.setIcon(new ImageIcon(VistaAcertijo1.class.getResource("/recursos_mapa/teclaBlanca.png")));
		SiCentral.setBounds(465, 184, 37, 89);
		contentPane.add(SiCentral);
		
		LaCentral = new JButton("");
		LaCentral.addActionListener(getControlador());
		LaCentral.setIcon(new ImageIcon(VistaAcertijo1.class.getResource("/recursos_mapa/teclaBlanca.png")));
		LaCentral.setBounds(430, 184, 37, 89);
		contentPane.add(LaCentral);
		
		SolCentral = new JButton("");
		SolCentral.addActionListener(getControlador());
		SolCentral.setIcon(new ImageIcon(VistaAcertijo1.class.getResource("/recursos_mapa/teclaBlanca.png")));
		SolCentral.setBounds(395, 184, 37, 89);
		contentPane.add(SolCentral);
		
		FaCentral = new JButton("");
		FaCentral.addActionListener(getControlador());
		FaCentral.setIcon(new ImageIcon(VistaAcertijo1.class.getResource("/recursos_mapa/teclaBlanca.png")));
		FaCentral.setBounds(360, 184, 37, 89);
		contentPane.add(FaCentral);
		
		MiCentral = new JButton("");
		MiCentral.addActionListener(getControlador());
		MiCentral.setIcon(new ImageIcon(VistaAcertijo1.class.getResource("/recursos_mapa/teclaBlanca.png")));
		MiCentral.setBounds(325, 184, 37, 89);
		contentPane.add(MiCentral);
		
		ReCentral = new JButton("");
		ReCentral.setIcon(new ImageIcon(VistaAcertijo1.class.getResource("/recursos_mapa/teclaBlanca.png")));
		ReCentral.addActionListener(getControlador());
		ReCentral.setBounds(290, 184, 37, 89);
		contentPane.add(ReCentral);
		
		DoCentral = new JButton("");
		DoCentral.addActionListener(getControlador());
		DoCentral.setIcon(new ImageIcon(VistaAcertijo1.class.getResource("/recursos_mapa/teclaBlanca.png")));
		DoCentral.setBounds(255, 184, 37, 89);
		contentPane.add(DoCentral);
		
		Reb = new JButton("");
		Reb.addActionListener(getControlador());
		Reb.setIcon(new ImageIcon(VistaAcertijo1.class.getResource("/recursos_mapa/teclaNegra.png")));
		Reb.setBounds(27, 11, 21, 173);
		contentPane.add(Reb);
		
		Mib = new JButton("");
		Mib.addActionListener(getControlador());
		Mib.setIcon(new ImageIcon(VistaAcertijo1.class.getResource("/recursos_mapa/teclaNegra.png")));
		Mib.setBounds(71, 11, 21, 173);
		contentPane.add(Mib);
		
		Solb = new JButton("");
		Solb.addActionListener(getControlador());
		Solb.setIcon(new ImageIcon(VistaAcertijo1.class.getResource("/recursos_mapa/teclaNegra.png")));
		Solb.setBounds(131, 11, 21, 173);
		contentPane.add(Solb);
		
		Lab = new JButton("");
		Lab.addActionListener(getControlador());
		Lab.setIcon(new ImageIcon(VistaAcertijo1.class.getResource("/recursos_mapa/teclaNegra.png")));
		Lab.setBounds(174, 11, 21, 173);
		contentPane.add(Lab);
		
		Sib = new JButton("");
		Sib.setIcon(new ImageIcon(VistaAcertijo1.class.getResource("/recursos_mapa/teclaNegra.png")));
		Sib.addActionListener(getControlador());
		Sib.setBounds(214, 11, 21, 173);
		contentPane.add(Sib);
		
		Si = new JButton("");
		Si.addActionListener(getControlador());
		Si.setIcon(new ImageIcon(VistaAcertijo1.class.getResource("/recursos_mapa/teclaBlanca.png")));
		Si.setBounds(220, 184, 37, 89);
		contentPane.add(Si);
		
		La = new JButton("");
		La.addActionListener(getControlador());
		La.setIcon(new ImageIcon(VistaAcertijo1.class.getResource("/recursos_mapa/teclaBlanca.png")));
		La.setBounds(185, 184, 37, 89);
		contentPane.add(La);
		
		Sol = new JButton("");
		Sol.addActionListener(getControlador());
		Sol.setIcon(new ImageIcon(VistaAcertijo1.class.getResource("/recursos_mapa/teclaBlanca.png")));
		Sol.setBounds(150, 184, 37, 89);
		contentPane.add(Sol);
		
		Fa = new JButton("");
		Fa.addActionListener(getControlador());
		Fa.setIcon(new ImageIcon(VistaAcertijo1.class.getResource("/recursos_mapa/teclaBlanca.png")));
		Fa.setBounds(115, 184, 37, 89);
		contentPane.add(Fa);
		
		Mi = new JButton("");
		Mi.addActionListener(getControlador());
		Mi.setIcon(new ImageIcon(VistaAcertijo1.class.getResource("/recursos_mapa/teclaBlanca.png")));
		Mi.setBounds(80, 184, 37, 89);
		contentPane.add(Mi);
		
		Re = new JButton("");
		Re.addActionListener(getControlador());
		Re.setIcon(new ImageIcon(VistaAcertijo1.class.getResource("/recursos_mapa/teclaBlanca.png")));
		Re.setBounds(45, 184, 37, 89);
		contentPane.add(Re);
		
		Do = new JButton("");
		Do.addActionListener(getControlador());
		Do.setIcon(new ImageIcon(VistaAcertijo1.class.getResource("/recursos_mapa/teclaBlanca.png")));
		Do.setBounds(10, 184, 37, 89);
		contentPane.add(Do);
		ImageIcon imageicon = new ImageIcon(new ImageIcon("assets/acertijo/piano.jpg").getImage().getScaledInstance(589,273,Image.SCALE_DEFAULT));
		setLocationRelativeTo(this);
	}

	public JButton getSibCentral() {
		return SibCentral;
	}

	public JButton getLabCentral() {
		return LabCentral;
	}

	public JButton getSolbCentral() {
		return SolbCentral;
	}

	public JButton getMibCentral() {
		return MibCentral;
	}

	public JButton getRebCentral() {
		return RebCentral;
	}

	public JButton getSiCentral() {
		return SiCentral;
	}

	public JButton getLaCentral() {
		return LaCentral;
	}

	public JButton getSolCentral() {
		return SolCentral;
	}

	public JButton getFaCentral() {
		return FaCentral;
	}

	public JButton getMiCentral() {
		return MiCentral;
	}

	public JButton getReCentral() {
		return ReCentral;
	}

	public JButton getDoCentral() {
		return DoCentral;
	}

	public JButton getReb() {
		return Reb;
	}

	public JButton getMib() {
		return Mib;
	}

	public JButton getSolb() {
		return Solb;
	}

	public JButton getLab() {
		return Lab;
	}

	public JButton getSib() {
		return Sib;
	}

	public JButton getSi() {
		return Si;
	}

	public JButton getLa() {
		return La;
	}

	public JButton getSol() {
		return Sol;
	}

	public JButton getFa() {
		return Fa;
	}

	public JButton getMi() {
		return Mi;
	}

	public JButton getRe() {
		return Re;
	}

	public JButton getDo() {
		return Do;
	}

	public ControladorAcertijo getControlador() {
		return controlador;
	}

	public void setControlador(ControladorAcertijo controlador) {
		this.controlador = controlador;
	}
	
	
}
