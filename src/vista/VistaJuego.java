  package vista;

import java.awt.*;
import java.awt.image.*;

import javax.swing.*;

import controlador.ControladorJuego;
import controlador.ControladorMenu;
import controlador.Teclado;
import modelo.Assets;
import modelo.Reproductor;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


public class VistaJuego extends JFrame implements Runnable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
    public int alto,ancho;

	public static final int WIDTH = 1200, HEIGHT = 720;
			
	public static  Boolean reproducir=true;
	private Canvas canvas;
	private Thread thread;
	private Boolean running = false;
	Reproductor rec;
	private BufferStrategy bufferstrategy;
	private Graphics2D g;
	
	private final int FPS = 33;
	private double targetTime = 1000000000/FPS;
	private double deltaSec = 0;
	
	private ControladorJuego controlador;
	private Teclado teclado;
	
	private String nombreUsuario;
	public static Boolean juegoTerminado= false;
	
	 public VistaJuego(String nombreUsuario) {

		 setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		 setSize(WIDTH, HEIGHT);
		 setLocationRelativeTo(null);
		 setTitle("Unnamed Game");
		 setResizable(false);
		 setVisible(true);
		 setBackground(Color.BLACK);
		 canvas = new Canvas();
		 canvas.setBounds(0, 0, 1194, 692);
		 teclado = new Teclado();
		 getContentPane().setLayout(null);
		 
		 JButton btnReinicio = new JButton("Reiniciar");
		 btnReinicio.addActionListener(new ActionListener() {
		 	public void actionPerformed(ActionEvent e) {
		 		reiniciarJuego();
		 	}
		 });
		 
		 JButton btnVolveraMenu = new JButton("Volver a men\u00FA");
		 btnVolveraMenu.addActionListener(new ActionListener() {
		 	public void actionPerformed(ActionEvent e) {
		 		volverAlMenu();
		 	}
		 });
		 btnVolveraMenu.setBounds(0, 658, 139, 23);
		 getContentPane().add(btnVolveraMenu);
		 btnReinicio.setBounds(1095, 658, 89, 23);
		 getContentPane().add(btnReinicio);
		 
		 canvas.setPreferredSize(new Dimension(WIDTH, HEIGHT));
		 canvas.setMaximumSize(new Dimension(WIDTH, HEIGHT));
		 canvas.setMinimumSize(new Dimension(WIDTH, HEIGHT));
		 canvas.setFocusable(true);
		 canvas.setBackground(Color.BLACK);
		 getContentPane().add(canvas);
		 canvas.addKeyListener(teclado);
		 
		 this.nombreUsuario = nombreUsuario;
		 
	 }
	 
	 private void update() {
		 
		 teclado.update();
		 controlador.update();
		 
	 }
	 
	 private void draw() {
		 
		 bufferstrategy = canvas.getBufferStrategy();
		 
		 if(bufferstrategy == null) {
			 canvas.createBufferStrategy(3);
			 return;
		 }
		 
		 g = (Graphics2D) bufferstrategy.getDrawGraphics();
		 ////////////////////////////////////
		 
		g.setColor(Color.BLACK);
		g.fillRect(0,0,WIDTH,HEIGHT);


		
		g.drawImage(controlador.getMapaActual(), 0, 0, WIDTH, HEIGHT,Color.black,this);
	   
		
		 controlador.draw(g);
		 		 
		 
		 ////////////////////////////////////
		 g.dispose();
		 bufferstrategy.show();
		 repaint();
	 }

	 
	 private void init() {
		 Assets.init();
		 controlador = new ControladorJuego(this.getNombreUsuario());
		 
	 }


	@Override
	public void run() {
		
		long now = 0;
		long lastTime = System.nanoTime();
		
		init();		
		
		while(running) {
			now = System.nanoTime();
			deltaSec += (now - lastTime)/targetTime;
			lastTime = now;
			
			if(deltaSec >= 1) {
				update();
				draw();
				
				if(juegoTerminado) {					
					volverAlMenu();					
				}
				
				deltaSec--;
			}
			
		}
		stop();
	}

	 @Override
	public void dispose() {
		super.dispose();
	}
	 
	 public void start() {
		 thread = new Thread(this);
		 thread.start();
		 running = true;
		 
	 }
	 
	 private void stop() {
		 try {
			thread.join();
			running = false;
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	 }

	 public String getNombreUsuario() {
			return nombreUsuario;
		}
	 
	 public void reiniciarJuego() {
		    dispose();
			running=false;
			new VistaJuego(nombreUsuario).start(); 	
	 }
	 
	 public void volverAlMenu() {
		    dispose();
			juegoTerminado=false;
			running=false;
			ControladorJuego.dificultad = "FACIL";
			new ControladorMenu();
	 }
}