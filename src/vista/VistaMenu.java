
package vista;

import javax.swing.*;
import javax.swing.RowSorter.SortKey;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

import java.awt.*;
import java.io.File;
import java.util.ArrayList;

import controlador.ControladorMenu;
import javazoom.jlgui.basicplayer.BasicPlayer;
import javazoom.jlgui.basicplayer.BasicPlayerException;

public class VistaMenu extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final int WIDTH = 640, HEIGHT = 480;
	private ControladorMenu controlador;
	private JButton btnEmpezar, btnDificultad, btnOpciones, btnSalir, btnFacil, btnMedia, btnDificil, btnGuia,
	btnVolver, btnRanking;
	private JLabel lblFondoMenu,labelguia;
	private JTable table;
	private DefaultTableModel modeloTabla;
	private JScrollPane scrollpane, scrollpane2;

	public VistaMenu(ControladorMenu controlador) {
		
			
		this.setControlador(controlador);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Menu Principal");
		setSize(WIDTH, HEIGHT);
		setBackground(Color.BLACK);
		setResizable(false);
		// setExtendedState(JFrame.MAXIMIZED_BOTH);
		setVisible(true);
		setLocationRelativeTo(null);
		
		btnEmpezar = new JButton("EMPEZAR");
		btnEmpezar.setFont(new Font("Yu Gothic Light", Font.BOLD | Font.ITALIC, 11));
		btnEmpezar.setBackground(Color.BLACK);
		btnEmpezar.setForeground(Color.RED);
		btnEmpezar.setBounds(270, 50, 120, 40);
		add(btnEmpezar);
		btnEmpezar.addActionListener(getControlador());

		btnOpciones = new JButton("OPCIONES");
		btnOpciones.setFont(new Font("Yu Gothic UI Light", Font.BOLD | Font.ITALIC, 11));
		btnOpciones.setForeground(Color.RED);
		btnOpciones.setBackground(Color.BLACK);
		btnOpciones.setBounds(270, 150, 120, 40);
		add(btnOpciones);
		btnOpciones.addActionListener(getControlador());
		
		btnRanking = new JButton("RANKING");
		btnRanking.setFont(new Font("Yu Gothic UI Light", Font.BOLD | Font.ITALIC, 11));
		btnRanking.setForeground(Color.RED);
		btnRanking.setBackground(Color.BLACK);
		btnRanking.setBounds(270, 250, 120, 40);
		add(btnRanking);
		btnRanking.addActionListener(getControlador());

		btnSalir = new JButton("SALIR");
		btnSalir.setFont(new Font("Yu Gothic UI Light", Font.BOLD | Font.ITALIC, 11));
		btnSalir.setForeground(Color.RED);
		btnSalir.setBackground(Color.BLACK);
		btnSalir.setBounds(270, 350, 120, 40);
		add(btnSalir);
		btnSalir.addActionListener(getControlador());
		

		btnDificultad = new JButton("DIFICULTAD");
		btnDificultad.setFont(new Font("Yu Gothic UI Light", Font.BOLD | Font.ITALIC, 11));
		btnDificultad.setForeground(Color.RED);
		btnDificultad.setBackground(Color.BLACK);
		btnDificultad.setVisible(false);
		btnDificultad.setBounds(270, 50, 120, 40);
		add(btnDificultad);
		btnDificultad.addActionListener(getControlador());

		btnFacil = new JButton("FACIL");
		btnFacil.setFont(new Font("Yu Gothic UI Light", Font.BOLD | Font.ITALIC, 11));
		btnFacil.setForeground(Color.RED);
		btnFacil.setBackground(Color.BLACK);
		btnFacil.setVisible(false);
		btnFacil.setBounds(270, 50, 120, 40);
		add(btnFacil);
		btnFacil.addActionListener(getControlador());

		btnMedia = new JButton("MEDIA");
		btnMedia.setFont(new Font("Yu Gothic UI Light", Font.BOLD | Font.ITALIC, 11));
		btnMedia.setForeground(Color.RED);
		btnMedia.setBackground(Color.BLACK);
		btnMedia.setVisible(false);
		btnMedia.setBounds(270, 150, 120, 40);
		add(btnMedia);
		btnMedia.addActionListener(getControlador());

		btnDificil = new JButton("DIFICIL");
		btnDificil.setFont(new Font("Yu Gothic UI Light", Font.BOLD | Font.ITALIC, 11));
		btnDificil.setForeground(Color.RED);
		btnDificil.setBackground(Color.BLACK);
		btnDificil.setVisible(false);
		btnDificil.setBounds(270, 250, 120, 40);
		add(btnDificil);
		btnDificil.addActionListener(getControlador());

		btnGuia = new JButton("GUIA");
		btnGuia.setFont(new Font("Yu Gothic UI Light", Font.BOLD | Font.ITALIC, 11));
		btnGuia.setForeground(Color.RED);
		btnGuia.setBackground(Color.BLACK);
		btnGuia.setVisible(false);
		btnGuia.setBounds(270, 150, 120, 40);
		add(btnGuia);
		btnGuia.addActionListener(getControlador());

		btnVolver = new JButton("VOLVER");
		btnVolver.setFont(new Font("Yu Gothic UI Light", Font.BOLD | Font.ITALIC, 11));
		btnVolver.setForeground(Color.RED);
		btnVolver.setBackground(Color.BLACK);
		btnVolver.setVisible(false);
		btnVolver.setBounds(270, 250, 120, 40);
		add(btnVolver);
		btnVolver.addActionListener(getControlador());
		
		


		String header[] = {"ID", "Usuario", "Tiempo", "Vidas", "Valoracion", "Puntos"};
		
		modeloTabla = new DefaultTableModel(null, header);
		
		table = new JTable(modeloTabla);
		table.setBounds(0, 0, 615, 480);
		table.setVisible(false);
		table.setDefaultEditor(Object.class, null);
		table.getColumnModel().getColumn(0).setMaxWidth(0);
		table.getColumnModel().getColumn(0).setMinWidth(0);
		table.getColumnModel().getColumn(0).setPreferredWidth(0);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		//table.getSelectionModel().addListSelectionListener(getControlador());
		
		ImageIcon imagenguia2 = new ImageIcon(getClass().getResource("/fondoMenu/guia.jpg"));
		labelguia = new JLabel(imagenguia2);
		scrollpane2 = new JScrollPane(table);
		scrollpane2.setBounds(5,5,615,390);
		scrollpane2.setVisible(false);
		add(scrollpane2);
		

		
		ImageIcon imagenguia = new ImageIcon(getClass().getResource("/fondoMenu/guia.jpg"));
		labelguia = new JLabel(imagenguia);
		scrollpane = new JScrollPane(labelguia);
		scrollpane.setBounds(5,5,620,390);
		scrollpane.setVisible(false);
		add(scrollpane);

		ImageIcon imagenfondo = new ImageIcon(getClass().getResource("/fondoMenu/fondo-textura-grunge_1048-11687.jpg"));
		lblFondoMenu = new JLabel(imagenfondo);
		lblFondoMenu.setBounds(0, 0, 640, 480);
		add(lblFondoMenu);
		
		 }

	  //para ordenar tabla
	//solo ordena por indice, no por valor completo
	/*public void rellenarTabla() {
		
		TableRowSorter<DefaultTableModel> sorter= new TableRowSorter<DefaultTableModel>(modeloTabla);
        ArrayList <SortKey> sortKeys= new ArrayList<>();
     
		
	}	*/
	
	public JButton getBtnDificultad() {
		return btnDificultad;
	}

	public JButton getBtnGuia() {
		return btnGuia;
	}

	public JButton getBtnVolver() {
		return btnVolver;
	}

	public ControladorMenu getControlador() {
		return controlador;
	}

	public void setControlador(ControladorMenu controlador) {
		this.controlador = controlador;
	}

	public JButton getBtnEmpezar() {
		return btnEmpezar;
	}

	public JButton getBtnOpciones() {
		return btnOpciones;
	}

	public JButton getBtnSalir() {
		return btnSalir;
	}
	
	public JButton getBtnFacil() {
		return btnFacil;
	}

	public JButton getBtnMedia() {
		return btnMedia;
	}

	public JButton getBtnDificil() {
		return btnDificil;
	}

	public JLabel getLabelguia() {
		return labelguia;
	}

	public JScrollPane getScrollpane() {
		return scrollpane;
	}

	public JButton getBtnRanking() {
		return btnRanking;
	}

	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}

	public JScrollPane getScrollpane2() {
		return scrollpane2;
	}

	public DefaultTableModel getModeloTabla() {
		return modeloTabla;
	}
	
	

}
